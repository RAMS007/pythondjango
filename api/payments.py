import logging
import traceback

import stripe
from django.conf import settings
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from api.base_permissions import CanMakePayments
from api.base_views import CustomAPIView
from api.choices import SubscriptionUserType, UserType
from api.models import (LicenseType, SecurityFirm, SecurityFirmSubscription,
                        SubscriptionPayment, Venue, VenueSubscription)
from api.serializers import StripeSourceSerializer
from api.utils import CustomException, success_response
from django.utils import timezone

logger = logging.getLogger(__name__)


stripe.api_key = settings.STRIPE_KEY

STRIPE_WEBHOOK_IPS = [
    '54.187.174.169',
    '54.187.205.235',
    '54.187.216.72',
    '54.241.31.99',
    '54.241.31.102',
    '54.241.34.107'
]


def create_stripe_customer(user_instance):
    stripe_customer = stripe.Customer.create(
        email=user_instance.email,
        name=user_instance.get_full_name()
    )
    return stripe_customer


def get_stripe_customer(user):
    if user.hospitality_id:
        organisation = user.hospitality
    else:
        organisation = user.security_firm
    if organisation.stripe_customer_id is None:
        stripe_customer = create_stripe_customer(user)
        organisation.stripe_customer_id = stripe_customer.id
        organisation.save(update_fields=['stripe_customer_id'])
    else:
        stripe_customer = stripe.Customer.retrieve(organisation.stripe_customer_id)
    return stripe_customer


def create_subscription(user, venue=None, security_firm=None):
    if venue:
        subscription_result = stripe.Subscription.create(
            customer=user.hospitality.stripe_customer_id,
            items=[{
                'plan': venue.license_type.subscription_plan_id,
                'metadata': {
                    'venue': str(venue.id)
                }
            }]
        )
    else:
        subscription_result = stripe.Subscription.create(
            customer=security_firm.stripe_customer_id,
            items=[{
                'plan': security_firm.license_type.subscription_plan_id,
                'metadata': {
                    'security_firm': str(security_firm.id)
                }
            }]
        )
    return subscription_result


def cancel_subscription(venue=None, security_firm=None):
    if venue:
        subscription_result = stripe.Subscription.delete(venue.current_subscription_id)
    else:
        subscription_result = stripe.Subscription.delete(
            security_firm.current_subscription_id)
    return subscription_result


class PaymentMethodAPI(CustomAPIView):
    permission_classes = (CanMakePayments,)

    def get_organisation(self):
        if self.request.user.hospitality_id:
            return self.request.user.hospitality
        else:
            return self.request.user.security_firm

    def post(self, request):
        customer = get_stripe_customer(self.request.user)
        source = customer.sources.create(source=request.data['token'])
        if request.data['make_default'] == 'True':
            stripe.Customer.modify(
                self.get_organisation().stripe_customer_id,
                default_source=source.id,
            )
        serializer = StripeSourceSerializer(source, context={'request': request})
        return success_response(data=serializer.data,
                                message='Payment source saved successfully')

    def get(self, request):
        customer = get_stripe_customer(self.request.user)
        sources = StripeSourceSerializer(customer.sources.auto_paging_iter(), many=True).data
        for source in sources:
            source['default'] = source['id'] == customer.default_source
        return success_response(data=sources)

    def patch(self, request):
        get_stripe_customer(self.request.user)
        customer = stripe.Customer.modify(
            self.get_organisation().stripe_customer_id,
            default_source=request.data['source_id'])
        sources = StripeSourceSerializer(customer.sources, many=True).data
        for source in sources:
            source['default'] = source['id'] == customer.default_source
        return success_response(message='Payment source updated successfully')

    def delete(self, request):
        get_stripe_customer(self.request.user)
        stripe.Customer.delete_source(
            self.get_organisation().stripe_customer_id,
            request.data['source_id']
        )
        return success_response(message='Payment source removed successfully')


class SubscriptionView(CustomAPIView):
    permission_classes = (CanMakePayments,)

    def get_venue(self):
        return Venue.objects.get(
            id=self.request.data['venue'], hospitality_id=self.request.user.hospitality_id)

    def get_security_firm(self):
        return SecurityFirm.objects.get(id=self.request.user.security_firm_id)

    def post(self, request):
        if request.user.user_type == UserType.hospitality_admin.value[0]:
            venue = self.get_venue()
            if venue.current_subscription_status == 'active':
                raise CustomException('Venue is already subscribed')
            if request.data.get('license_type'):
                venue.license_type = LicenseType.objects.get(
                    id=self.request.data['license_type'],
                    subscription_type=SubscriptionUserType.hospitality_admin.value[0])
                venue.save(update_fields=['license_type'])
            subscription_data = create_subscription(request.user, venue=venue)
            message = 'Venue has been subscribed successfully'
        else:
            security_firm = self.get_security_firm()
            if security_firm.current_subscription_status == 'active':
                raise CustomException('Your Firm is already subscribed')
            security_firm.license_type = LicenseType.objects.get(
                id=self.request.data['license_type'],
                subscription_type=SubscriptionUserType.security_admin.value[0])
            security_firm.save(update_fields=['license_type'])
            subscription_data = create_subscription(request.user, security_firm=security_firm)
            message = 'Your firm has been subscribed successfully'
        return success_response(
            message=message,
            data={
                'subscription_current_period_end': timezone.datetime.fromtimestamp(subscription_data['current_period_end']),
                'license_type_id': self.request.data['license_type'],
                'is_active_subscription': subscription_data['status'] == 'active',
            })

    def delete(self, request):
        if request.user.user_type == UserType.hospitality_admin.value[0]:
            venue = self.get_venue()
            if venue.current_subscription_status == 'canceled':
                raise CustomException('Venue is already unsubscribed')
            cancel_subscription(venue=venue)
            message = 'Venue has been unsubscribed successfully'
        else:
            security_firm = self.get_security_firm()
            if security_firm.current_subscription_status == 'canceled':
                raise CustomException('Your Firm is already unsubscribed')
            cancel_subscription(security_firm=security_firm)
            message = 'Your firm has been unsubscribed successfully'
        return success_response(message=message)


class StripeWebhook(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            event = stripe.Webhook.construct_event(
                request.body, request.META['HTTP_STRIPE_SIGNATURE'], settings.STRIPE_SIGNING_SECRET
            )
            logger.info({
                'ref': 'Request data in stripe webhook',
                'req_data': request.data,
                'event_type': event.type
            })
            if event.type == 'customer.subscription.created':
                metadata = request.data['data']['object']['items']['data'][0]['metadata']
                if metadata.get('venue'):
                    Venue.objects.filter(id=metadata['venue']).update(
                        current_subscription_id=request.data['data']['object']['id'],
                        current_subscription_status=request.data['data']['object']['status'],
                        subscription_current_period_end=timezone.datetime.fromtimestamp(request.data['data']['object']['current_period_end']))
                    VenueSubscription.objects.create(
                        venue_id=metadata['venue'],
                        plan_id=request.data['data']['object']['plan']['id'],
                        subscription_id=request.data['data']['object']['id'],
                        status=request.data['data']['object']['status'])
                elif metadata.get('security_firm'):
                    SecurityFirm.objects.filter(id=metadata['security_firm']).update(
                        current_subscription_id=request.data['data']['object']['id'],
                        current_subscription_status=request.data['data']['object']['status'],
                        subscription_current_period_end=timezone.datetime.fromtimestamp(request.data['data']['object']['current_period_end']))
                    SecurityFirmSubscription.objects.create(
                        security_firm_id=metadata['security_firm'],
                        plan_id=request.data['data']['object']['plan']['id'],
                        subscription_id=request.data['data']['object']['id'],
                        status=request.data['data']['object']['status'])
            if event.type in ['customer.subscription.updated',
                              'customer.subscription.deleted']:
                metadata = request.data['data']['object']['items']['data'][0]['metadata']
                if metadata.get('venue'):
                    Venue.objects.filter(
                        id=metadata['venue'],
                        current_subscription_id=request.data['data']['object']['id']
                    ).update(
                        current_subscription_status=request.data['data']['object']['status'],
                        subscription_current_period_end=timezone.datetime.fromtimestamp(request.data['data']['object']['current_period_end']))
                    VenueSubscription.objects.filter(subscription_id=request.data['data']['object']['id']).update(
                        status=request.data['data']['object']['status'])
                elif metadata.get('security_firm'):
                    SecurityFirm.objects.filter(
                        id=metadata['security_firm'],
                        current_subscription_id=request.data['data']['object']['id']
                    ).update(
                        current_subscription_status=request.data['data']['object']['status'],
                        subscription_current_period_end=timezone.datetime.fromtimestamp(request.data['data']['object']['current_period_end']))
                    SecurityFirmSubscription.objects.filter(subscription_id=request.data['data']['object']['id']).update(
                        status=request.data['data']['object']['status'])
            if event.type == 'invoice.payment_succeeded':
                SubscriptionPayment.objects.create(
                    subscription_id=request.data['data']['object']['subscription'],
                    amount=(request.data['data']['object']['amount_paid'] / 100))
            return Response('OK')
        except Exception as e:
            logger.error({
                'ref': 'Error in stripe webhook',
                'error': str(e),
                'traceback': str(traceback.format_exc())
                })
            return Response('Error occurred', status.HTTP_500_INTERNAL_SERVER_ERROR)
