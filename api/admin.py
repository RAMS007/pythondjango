from django.contrib import admin
from api.models import CustomUser, LicenseType
from django.contrib.auth.admin import UserAdmin

admin.site.site_header = 'Star Compliance'
admin.site.site_url = None

admin.site.register(LicenseType)

@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions', 'user_type'),
        }),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    ordering = ('first_name', 'email')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('first_name', 'last_name', 'email')
