from api.utils import random_file_name
from django.utils import timezone


def get_extension(filename):
    return f".{filename.split('.')[-1]}"


def get_updated_filename(filename):
    split_filename = filename.split('.')
    no_ext_filename = ''.join([x for x in split_filename[:len(split_filename) - 1]])
    return f'{no_ext_filename}-{int(timezone.now().timestamp() * 1000)}{get_extension(filename)}'


def handle_user_image(instance, filename):
    # filename = get_updated_filename(filename)
    return "/".join(['users', instance.id.hex, filename])


def handle_venue_doc(instance, filename):
    # filename = get_updated_filename(filename)
    return "/".join(['venues_docs', instance.venue_id.hex, filename])


def handle_venue_logo(instance, filename):
    # filename = get_updated_filename(filename)
    return "/".join(['venues', instance.id.hex, filename])


def handle_user_doc(instance, filename):
    # filename = get_updated_filename(filename)
    return "/".join(['user_docs', instance.user_id.hex, filename])


def handle_incident_files(instance, filename):
    # filename = get_updated_filename(filename)
    return "/".join(['incidents', instance.incident_id.hex, filename])


def handle_checklist_files(instance, filename):
    return "/".join(['checklists', instance.checklist_id.hex, filename])