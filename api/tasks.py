import csv
import itertools
import logging
import os
import uuid
from io import StringIO

import boto3
import firebase_admin
import pdfkit
import pytz
import sendgrid
from dateutil.parser import parse
from django.conf import settings
from django.db import connection
from django.db.models import F, Prefetch, Q
from django.template.loader import render_to_string
from django.utils import timezone
from firebase_admin import credentials, messaging
from rest_framework_simplejwt.backends import TokenBackend
from sendgrid import From, To
from sendgrid.helpers.mail import Content, Mail, Personalization

from api.choices import DeviceType, NotificationType
from api.models import (CustomUser, Incident, IncidentLog, IncidentMedia,
                        IncidentPerson, UserNotification, CovidChecklist,
                        Venue, CovidChecklistType, ChecklistImage)
from api.query_utils import validate_security_license
from api.utils import ALL_SECURITY_ROLES
from star_compliance.celery import app

s3 = boto3.resource('s3')
bucket = s3.Bucket(settings.AWS_STORAGE_BUCKET_NAME)

logger = logging.getLogger(__name__)


cred = credentials.Certificate(os.path.join(settings.BASE_DIR, 'service-account.json'))
firebase_admin.initialize_app(cred)


@app.task()
def send_fcm_to_token(token, data, notification):
    try:
        if notification:
            message = messaging.Message(
                data=data,
                notification=messaging.Notification(**notification),
                token=token,
            )
        else:
            message = messaging.Message(
                data=data,
                token=token,
            )

        response = messaging.send(message)
        logger.info({
            'ref': 'Push message sent successfully',
            'sending_data': data,
            'response_json': str(response),
        })
    except Exception as e:
        logger.error({
            'ref': f'Error while fcm send',
            'sending_data': data,
            'error': str(e)
        })


@app.task
def send_sendgrid_mail(to_mail, subject, html_text='', template_id=None, substitutes={}):
    sg = sendgrid.SendGridAPIClient(api_key=settings.SENDGRID_API_KEY)
    from_email = From(email=settings.SENDGRID_EMAIL_ID, name=settings.SENDGRID_FROM_NAME)
    subject = subject
    to_email = To(to_mail)
    content = Content("text/html", html_text)
    mail = Mail(
        from_email=from_email,
        subject=subject,
        html_content=content,
        to_emails=to_email)
    if template_id:
        mail.template_id = template_id
        mail.personalizations[0].dynamic_template_data = substitutes
    response = sg.client.mail.send.post(request_body=mail.get())
    logger.info({
        'ref': 'Mail sent successfully',
        'sent_to': to_mail,
        'subject': subject,
        'response_status_code': str(response.status_code),
        'response_body': response.body
    })
    return 'mail sent, response code: {}'.format(response.status_code)


@app.task
def send_batch_mail(to_mail, subject, html_text):
    sg = sendgrid.SendGridAPIClient(api_key=settings.SENDGRID_API_KEY)
    from_email = From(email=settings.SENDGRID_EMAIL_ID, name=settings.SENDGRID_FROM_NAME)
    subject = subject
    content = Content("text/html", html_text)
    mail = Mail(
        from_email=from_email,
        subject=subject,
        html_content=content,
        to_emails=To(
            to_mail[0]))
    if to_mail[1:]:
        personalization = Personalization()
        for i in to_mail[1:]:
            personalization.add_to(To(i))
        mail.add_personalization(personalization)
    response = sg.client.mail.send.post(request_body=mail.get())
    logger.info({
        'ref': 'Response from batch mail send',
        'sending_to_emails': to_mail,
        'subject': subject,
        'reponse_status_code': response.status_code,
        'response_body': response.body
    })
    return 'batch mail sent, response code: {}'.format(response.status_code)


@app.task()
def send_notification_to_users(user_ids, notification_type, title, body, extra_data={}):
    logger.info({
        'ref': 'Request received to send notifications to users',
        'user_ids': user_ids,
        'notification_type': notification_type,
        'title': title,
        'body': body,
        'extra_data': extra_data,
    })
    if user_ids is None:  # meaning notification was sent to all users
        users = CustomUser.objects.all().prefetch_related('devicetoken_set')
    else:
        users = CustomUser.objects.filter(
            id__in=user_ids).prefetch_related('devicetoken_set')
    id_data = extra_data.get('id_data')
    for user in users:
        notification = UserNotification.objects.create(
            notification_type=notification_type,
            user=user,
            title=title,
            body=body,
            id_data=id_data)
        CustomUser.objects.filter(id=user.id).update(
            unread_notification_count=F('unread_notification_count') + 1)
        if user.notification_setting:
            data = {
                'notification_type': notification_type,
                'title': title,
                'body': body
            }
            data.update(extra_data)
            notification = {
                'title': title,
                'body': body
            }
            for device_token in user.devicetoken_set.all():
                if device_token.device_type == DeviceType.web.value[0]:
                    send_fcm_to_token(device_token.token, data, None)
                else:
                    send_fcm_to_token(device_token.token, data, notification)
    return f'Sent {notification_type} notifications to users'


def get_user_name_info(user):
    return f'{user.get_full_name()}({user.email} [{user.get_user_type_display()}])'


@app.task
def create_incident_report(query_sql_params, to_email, host_name,
                           report_type, happened_from, happened_to):
    """
    report type: CSV or PDF
    """
    if happened_from and happened_to:
        happened_from = f"{timezone.datetime.strftime(timezone.localtime(parse(happened_from), pytz.timezone('Australia/Sydney')), '%d/%m/%Y %I:%M %p')} AEST"
        happened_to = f"{timezone.datetime.strftime(timezone.localtime(parse(happened_to), pytz.timezone('Australia/Sydney')), '%d/%m/%Y %I:%M %p')} AEST"

    logger.info({
        'ref': 'Request received to create incident report',
        'requested_by_user': to_email,
        'sql_query': query_sql_params,
        'report_type': report_type
    })
    with connection.cursor() as cursor:
        cursor.execute(*query_sql_params)
        incident_ids = cursor.fetchall()
    incident_ids = list(itertools.chain(*incident_ids))
    if report_type == 'CSV':
        incidents = Incident.objects.filter(
            id__in=incident_ids).select_related(
            'venue', 'venue_manager', 'drafted_by', 'submitted_by', 'approved_by', 'linked_incident'
        ).prefetch_related('actions', 'incident_persons').order_by('venue__name', '-modified_at',)
        string_obj = StringIO()
        csv_writer = csv.writer(
            string_obj,
            delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([
            'UNIQUE ID',
            'VENUE',
            'MANAGER',
            'CURRENT STATUS',
            'DRAFTED BY',
            'SUBMITTED BY',
            'APPROVED BY',
            'HAPPENED AT (UTC)',
            'LOCATION',
            'CATEGORIES',
            'ACTIONS TAKEN',
            'SUMMARY',
            'IS FOOTAGE ON CCTV?',
            'IS FOOTAGE ON DISC?',
            'LINKED INCIDENT',
            'INVOLVED PERSON COUNT',
            'PERSON NAME',
            'PERSON AGE',
            'PERSON EMAIL',
            'PERSON GENDER',
            'PERSON LICENSE NUMBER',
            'PERSON PHONE NUMBER',
            'PERSON BUILD',
            'PERSON HEIGHT',
            'PERSON HAIRS',
            'PERSON TOP CLOTHING',
            'PERSON BOTTOM CLOTHING',
            'PERSON SHOES',
            'PERSON OTHER DESCRIPTION',
            'PERSON WEAPONS',
            'PERSON INJURY DESCRIPTION',
            'IS PERSON A WITNESS?'
        ])
        for incident in incidents:
            csv_writer.writerow([
                incident.unique_id,
                incident.venue.name,
                get_user_name_info(
                    incident.venue_manager) if incident.venue_manager else '',
                incident.get_current_status_display(),
                get_user_name_info(incident.drafted_by) if incident.drafted_by else '',
                get_user_name_info(
                    incident.submitted_by) if incident.submitted_by else '',
                get_user_name_info(incident.approved_by) if incident.approved_by else '',
                incident.happened_at,
                incident.location,
                incident.categories,
                [x.title for x in incident.actions.all()],
                incident.summary,
                incident.footage_on_cctv,
                incident.footage_on_disc,
                incident.linked_incident.unique_id if incident.linked_incident else '',
                incident.person_count,
            ])
            for index, person in enumerate(incident.incident_persons.all()):
                csv_writer.writerow([
                    '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                    person.name,
                    person.age,
                    person.email,
                    person.get_gender_display(),
                    person.license_number,
                    person.phone_number,
                    person.is_witness,
                    person.build,
                    person.height,
                    person.hair,
                    person.top_clothing,
                    person.bottom_clothing,
                    person.shoes,
                    person.other_description,
                    person.weapon,
                    person.injury_description,
                ])
        csv_writer.writerow([''])
        csv_writer.writerow([''])
        report_body = string_obj.getvalue()
    else:
        incidents = Incident.objects.filter(
            id__in=incident_ids).select_related('venue', 'drafted_by').prefetch_related('actions')
        venue_wise_data = {}
        for incident in incidents:
            setattr(incident, 'actions_list', ', '.join(
                [x.title for x in incident.actions.all()]))
            if incident.venue not in venue_wise_data:
                venue_wise_data[incident.venue] = []
            venue_wise_data[incident.venue].append(incident)
        report_html_string = render_to_string(
            'reports/incident_listing.html',
            context={
                'happened_from': happened_from,
                'happened_to': happened_to,
                'venue_wise_data': venue_wise_data
            }
        )
        options = {
            'header-html': f'{settings.AWS_S3_BASE_URL}/incident_listing_header.html',
            'orientation': 'landscape',
            # 'margin-bottom': 10,
            'margin-left': 1,
            'margin-right': 1,
            'margin-top': 35,
            'footer-right': 'Page [page]/[topage]',
            'footer-left': 'Copyright HJM Security Pty Ltd,All Rights Reserved.Master Licence Number: 000104611\nContact: info@starcomply.com.au',
            'footer-font-size': 8,
            'footer-spacing': 4,
        }
        report_body = pdfkit.from_string(report_html_string, False, options)

    key = f'reports/incidents_report_{int(timezone.now().timestamp())}.{"csv" if report_type == "CSV" else "pdf"}'
    bucket.put_object(Body=report_body, Key=key)
    subject = 'Incident report'
    token = TokenBackend(
        settings.SIMPLE_JWT['ALGORITHM'],
        settings.SIMPLE_JWT['SIGNING_KEY']).encode({
            'key': key
        })
    link = f'{host_name}/download-report/?token={token}'
    substitutes = {
        'link': link,
    }
    send_sendgrid_mail(to_mail=to_email,
                       subject=subject,
                       html_text='Star Compliance',
                       template_id=settings.INCIDENT_REPORT_TEMPLATE,
                       substitutes=substitutes)
    return 'Incident report created successfully'


@app.task
def create_incident_detail_report(incident_id, to_email, host_name):
    logger.info({
        'ref': 'Request received to create detailed incident report',
        'requested_by_user': to_email,
        'incident_id': incident_id
    })
    try:
        incident = Incident.objects.get(id=uuid.UUID(incident_id))
    except ValueError:
        incident = Incident.objects.get(unique_id=incident_id)
    if incident.drafted_by.user_type in ALL_SECURITY_ROLES:
        organisation_name = incident.drafted_by.security_firm.name.upper()
        organisation_logo = f'{settings.AWS_S3_BASE_URL}/security.png'
        verified_logo = f'{settings.AWS_S3_BASE_URL}/right.png' if incident.drafted_by.is_license_active else f'{settings.AWS_S3_BASE_URL}/red_cross.png'
    else:
        organisation_name = incident.drafted_by.hospitality.name.upper()
        organisation_logo = incident.venue.logo.url if incident.venue.logo else ''
        verified_logo = None
    incident_persons = IncidentPerson.objects.filter(incident_id=incident.id)
    report_html_string = render_to_string(
        'reports/incident_detail.html',
        context={
            'incident': incident,
            'organisation_name': organisation_name,
            'organisation_logo': organisation_logo,
            'verified_logo': verified_logo,
            'incident_logs': IncidentLog.objects.filter(incident_id=incident.id),
            'actions_list': ', '.join(list(incident.actions.values_list('title', flat=True))),
            'incident_persons': incident_persons,
            'image_base_url': settings.AWS_S3_BASE_URL,
            'incident_medias': IncidentMedia.objects.filter(incident_id=incident.id)
        }
    )
    options = {
        'header-html': f'{settings.AWS_S3_BASE_URL}/incident_detail_header.html',
        # 'margin-bottom': 8,
        'margin-left': 2,
        'margin-right': 2,
        'margin-top': 35,
        'footer-right': ' Page [page]/[topage]',
        'footer-left': 'Copyright HJM Security Pty Ltd,All Rights Reserved.Master Licence Number: 000104611\nContact: info@starcomply.com.au',
        'footer-font-size': 8,
        'footer-spacing': 4,
    }
    report_body = pdfkit.from_string(report_html_string, False, options)

    key = f'reports/incident_detail_report_{int(timezone.now().timestamp())}.pdf'
    bucket.put_object(Body=report_body, Key=key)
    subject = 'Incident detail report'
    token = TokenBackend(
        settings.SIMPLE_JWT['ALGORITHM'],
        settings.SIMPLE_JWT['SIGNING_KEY']).encode({
            'key': key
        })
    link = f'{host_name}/download-report/?token={token}'
    substitutes = {
        'link': link,
    }
    send_sendgrid_mail(to_mail=to_email,
                       subject=subject,
                       html_text='Star Compliance',
                       template_id=settings.INCIDENT_REPORT_TEMPLATE,
                       substitutes=substitutes)
    return 'Incident detailed report created and sent to the user'


@app.task
def send_admin_error_alert(occurred_while, details):
    kwargs = {
        'to_mail': list(CustomUser.objects.filter(is_superuser=True).values_list('email', flat=True)),
        'subject': 'Error occured in Star Compliance',
        'html_text': render_to_string('emails/admin_alert.html', context={
            'while': occurred_while,
            'details': details
        })
    }
    send_batch_mail(**kwargs)
    return f'Alert sent to admins for, {occurred_while}'


@app.task
def notify_license_expiry():
    """
    Periodic - daily
    Check for the expiration of the license 6 months, 3 months and 7 days from today. And send notification to renew.
    """
    users_to_notify = []
    six_month_date = timezone.localdate() + timezone.timedelta(days=30 * 6)
    three_month_date = timezone.localdate() + timezone.timedelta(days=30 * 3)
    week_date = timezone.localdate() + timezone.timedelta(days=7)
    expiring_license_users = CustomUser.objects.filter(
        Q(license_expiry=six_month_date) |
        Q(license_expiry=three_month_date) |
        Q(license_expiry=week_date)
    ).values('license_number', 'id')
    for user in expiring_license_users:
        expiry_date, license_type = validate_security_license(user['license_number'])
        if expiry_date:
            CustomUser.objects.filter(id=user['id']).update(license_expiry=expiry_date)
            if expiry_date < timezone.localdate():
                users_to_notify.append(user['id'])
        else:
            continue
    send_notification_to_users(
        users_to_notify,
        NotificationType.license_expiring.value[0],
        'Your security license is going to expire',
        'Please renew your license to keep your profile verified'
    )


@app.task()
def create_checklist_report(qs_query_params, email, host, venue_id, checklist_type, from_date, to_date):
    with connection.cursor() as cursor:
        cursor.execute(*qs_query_params)
        checklist_ids = cursor.fetchall()
    checklist_ids = list(itertools.chain(*checklist_ids))
    checklists = []
    from_index = 0
    gap = 5
    to_index = gap
    while (to_index < (len(checklist_ids) + gap)) or (not from_index):
        checklists.extend(list(
            CovidChecklist.objects.filter(
                id__in=checklist_ids
                ).select_related('user').prefetch_related('checklistimage_set')[from_index:to_index]))
        from_index = to_index
        to_index = to_index + gap
    
    report_html_string = render_to_string(
        'reports/covid_checklist.html',
        context={
            'venue': Venue.objects.get(id=venue_id),
            'checklist_type': next(x.value[1] for x in CovidChecklistType if int(x.value[0]) == int(checklist_type)),
            'image_base_url': settings.AWS_S3_BASE_URL,
            'checklists': checklists,
            'from_date': parse(from_date) if from_date else None,
            'to_date': parse(to_date) if to_date else None,
        }
    )
    options = {
        'header-html': f'{settings.AWS_S3_BASE_URL}/covid_checklist_header.html',
        # 'margin-bottom': 8,
        'margin-left': 2,
        'margin-right': 2,
        'margin-top': 35,
        'footer-center': ' Page [page]/[topage]',
        # 'footer-left': 'Copyright HJM Security Pty Ltd,All Rights Reserved.Master Licence Number: 000104611\nContact: info@starcomply.com.au',
        'footer-font-size': 8
    }
    report_body = pdfkit.from_string(report_html_string, False, options)

    key = f'reports/covid_checklist_report_{int(timezone.now().timestamp())}.pdf'
    bucket.put_object(Body=report_body, Key=key)
    subject = 'Covid checklist report'
    token = TokenBackend(
        settings.SIMPLE_JWT['ALGORITHM'],
        settings.SIMPLE_JWT['SIGNING_KEY']).encode({
            'key': key
        })
    link = f'{host}/download-report/?token={token}'
    substitutes = {
        'link': link,
    }
    send_sendgrid_mail(to_mail=email,
                       subject=subject,
                       html_text='Star Compliance',
                       template_id=settings.COVID_CHECKLIST_REPORT_TEMPLATE,
                       substitutes=substitutes)
    return