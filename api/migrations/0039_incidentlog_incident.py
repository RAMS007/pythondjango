# Generated by Django 2.2.5 on 2019-11-28 11:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0038_incidentlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='incidentlog',
            name='incident',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Incident'),
        ),
    ]
