# Generated by Django 2.2.5 on 2019-10-14 07:28

import api.upload_handlers
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0010_auto_20191003_1313'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hospitality',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=300)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('phone', models.CharField(blank=True, max_length=50, null=True)),
                ('stripe_customer_id', models.TextField(blank=True, null=True)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='UserDocument',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('document', models.FileField(upload_to=api.upload_handlers.handle_user_doc)),
            ],
            options={
                'ordering': ('-created_at',),
            },
        ),
        migrations.RemoveField(
            model_name='securityfirmdocument',
            name='security_firm',
        ),
        migrations.RemoveField(
            model_name='securityfirmsubscription',
            name='security_firm',
        ),
        migrations.RemoveField(
            model_name='venue',
            name='hospitality_admin',
        ),
        migrations.RemoveField(
            model_name='venue',
            name='license_type',
        ),
        migrations.RemoveField(
            model_name='venue',
            name='security_firm',
        ),
        migrations.RemoveField(
            model_name='venuedocument',
            name='venue',
        ),
        migrations.AlterModelOptions(
            name='usernotification',
            options={'ordering': ('-created_at',)},
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='company_name',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='stripe_customer_id',
        ),
        migrations.DeleteModel(
            name='SecurityFirm',
        ),
        migrations.AddField(
            model_name='customuser',
            name='address',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='competency_card_number',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='emergency_contact_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='emergency_contact_phone',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='phone_number',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='postcode',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='state',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='customuser',
            name='suburb',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
        migrations.DeleteModel(
            name='LicenseType',
        ),
        migrations.DeleteModel(
            name='SecurityFirmDocument',
        ),
        migrations.DeleteModel(
            name='SecurityFirmSubscription',
        ),
        migrations.DeleteModel(
            name='Venue',
        ),
        migrations.DeleteModel(
            name='VenueDocument',
        ),
        migrations.AddField(
            model_name='userdocument',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='customuser',
            name='hospitality',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.Hospitality'),
        ),
    ]
