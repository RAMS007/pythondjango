# Generated by Django 2.2.5 on 2019-10-15 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_auto_20191015_1612'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='venuesubscriptionpayment',
            name='subscription',
        ),
        migrations.AddField(
            model_name='venuesubscriptionpayment',
            name='subscription_id',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
