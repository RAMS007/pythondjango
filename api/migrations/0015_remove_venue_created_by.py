# Generated by Django 2.2.5 on 2019-10-15 10:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_licensetype_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='venue',
            name='created_by',
        ),
    ]
