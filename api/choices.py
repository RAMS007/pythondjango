from enum import Enum


class ChoiceEnum(Enum):
    @classmethod
    def get_value(cls, member):
        return cls[member].value[0]


class NotificationType(ChoiceEnum):
    tnc_updated = ('TNC', 'Terms and conditions updated')
    legal_terms_updated = ('LTU', 'Legal terms updated')
    payment_terms_updated = ('PTU', 'Payment terms updated')
    privacy_policy_updated = ('PPU', 'Privacy policy updated')
    incident_drafted = ('INCIDENT_DRAFTED', 'Incident drafted')
    incident_submitted = ('INCIDENT_SUBMITTED', 'Incident submitted')
    incident_approved = ('INCIDENT_APPROVED', 'Incident approved')
    license_expiring = ('LICENSE_EXPIRING', 'License is close to expiration')


class DeviceType(ChoiceEnum):
    ios = ('IOS', 'Iphone')
    android = ('ANDROID', 'Android')
    web = ('WEB', 'Web')


class UserType(ChoiceEnum):
    hospitality_admin = ('HOSPITALITY_ADMIN', 'Hospitality Admin')
    venue_admin = ('VENUE_ADMIN', 'Venue admin')
    venue_manager = ('VENUE_MANAGER', 'Venue manager')
    venue_staff = ('VENUE_STAFF', 'Venue staff')
    security_admin = ('SECURITY_ADMIN', 'Security admin')
    security_manager = ('SECURITY_MANAGER', 'Security manager')
    security_staff = ('SECURITY_STAFF', 'Security staff')


class SubscriptionUserType(ChoiceEnum):
    security_admin = ('SA', 'Security Admin')
    hospitality_admin = ('HA', 'Hospitality admin')


class Gender(ChoiceEnum):
    male = ('M', 'Male')
    female = ('F', 'Female')
    other = ('O', 'Other')


class IncidentStatus(ChoiceEnum):
    local_draft = (0, 'DRAFTED LOCALLY')
    published_draft = (1, 'DRAFTED')
    submitted = (2, 'SUBMITTED')
    approved = (3, 'APPROVED')


class IncidentLogType(ChoiceEnum):
    drafted = (0, 'Incident Drafted')
    submitted = (1, 'Incident Submitted')
    approved = (2, 'Incidnet Approved')
    edited = (3, 'Incidnet Edited')
    person_added = (4, 'New Person Added to incident')
    person_removed = (5, 'A person removed from incident')
    person_updated = (6, 'A person in incident updated')
    media_added = (7, 'A media added to incident')
    media_removed = (8, 'A media removed from incident')
    

class CovidChecklistType(ChoiceEnum):
    health_hygiene = (1, 'Health Hygiene and facilities checklist')
    cleaning = (2, 'Cleaning checklist')
    social_distancing = (3, 'Social Distancing checklist')
