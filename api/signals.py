from api.models import CustomUser, SecurityFirm, Hospitality, Venue, LicenseType, IncidentLog
from django.dispatch import receiver
from django.db.models.signals import post_save
from api.payments import get_stripe_customer
from api.choices import UserType
from api.utils import get_welcome_email_template_id, CustomException
from api.tasks import send_sendgrid_mail
from django.db import transaction
from django.conf import settings
from api.payments import stripe


@receiver(post_save, sender=CustomUser)
def after_user_save(**kwargs):
    instance = kwargs['instance']
    if instance.user_type == UserType.hospitality_admin.value[0]:
        if not instance.hospitality.stripe_customer_id:
            get_stripe_customer(instance)


@receiver(post_save, sender=Venue)
def save_prefix_id(**kwargs):
    instance = kwargs['instance']
    if not instance.prefix_id:
        prefix = ''.join(x[0] for x in instance.name.split()).upper()
        flag = 1
        while flag:
            if Venue.objects.filter(prefix_id__iexact=prefix).exists():
                if flag == 1:
                    prefix += str(flag)
                else:
                    prefix = prefix[:len(prefix) - 1] + str(flag)
                flag += 1
            else:
                flag = 0
        Venue.objects.filter(id=instance.id).update(prefix_id=prefix)


@receiver(post_save, sender=LicenseType)
def check_plan_id(**kwargs):
    instance = kwargs['instance']
    try:
        stripe.Plan.retrieve(instance.subscription_plan_id)
    except Exception as e:
        raise CustomException('Please enter a valid stripe subscription plan id')
