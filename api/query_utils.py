import requests
from django.conf import settings
from django.utils import timezone

from api.models import NSWGovAPIToken, SecurityLicense


def get_nswgov_bearer_token():
    active_token = NSWGovAPIToken.objects.filter(expires_at__gt=timezone.now()).first()
    if active_token:
        return active_token.token
    else:
        url = 'https://api.onegov.nsw.gov.au/oauth/client_credential/accesstoken'
        params = {
            'grant_type': 'client_credentials'
        }
        r = requests.get(
            url,
            params=params,
            auth=(
                settings.NSW_GOV_SECURITY_API_KEY,
                settings.NSW_GOV_SECURITY_API_SECRET))
        if r.status_code == 200:
            token_response = r.json()
            NSWGovAPIToken.objects.create(
                token=token_response['access_token'],
                expires_at=timezone.now().fromtimestamp(
                    int((float(token_response['issued_at']) /
                         1000) + float(token_response['expires_in']))
                ))
            return token_response['access_token']
        else:
            logger.error({
                'ref': 'Error in NSW gov access token API',
                'detail': str(r.json())
            })
            send_admin_error_alert.apply_async(kwargs={
                'occurred_while': 'Calling NSW Gov API for oauth token',
                'details': str(r.json())
            })
            return None


def validate_security_license(license_number):
    """Returns, expiry_date, license_type"""
    access_token = get_nswgov_bearer_token()
    if access_token:
        url = 'https://api.onegov.nsw.gov.au/securityregister/v1/verify'
        params = {
            'licenceNumber': license_number
        }
        headers = {
            'Accept': 'application/json',
            'dataType': 'json',
            'apikey': settings.NSW_GOV_SECURITY_API_KEY,
            'Authorization': f'Bearer {access_token}',
            'Content-Type': 'application/json; charset=utf-8'
        }
        r = requests.get(url, params=params, headers=headers)
        if r.status_code == 200:
            license_data = r.json()
            if license_data:
                SecurityLicense.objects.update_or_create(
                    license_number=license_data[0]['licenceNumber'],
                    defaults={
                        'expiry_date': timezone.datetime.strptime(license_data[0]['expiryDate'], '%d/%m/%Y').date(),
                        'license_type': license_data[0]['licenceType']
                    })
                return (timezone.datetime.strptime(license_data[0]['expiryDate'], '%d/%m/%Y').date(),
                        license_data[0]['licenceType'])
    return (None, None)
