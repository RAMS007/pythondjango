import base64
import math
import os
import uuid
import random
import string
import datetime
import requests

from collections import OrderedDict
from io import BytesIO
from tempfile import SpooledTemporaryFile

from django.db.models.fields.files import ImageFieldFile
from django.conf import settings
from django.contrib.auth.password_validation import \
    get_default_password_validators
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import TemporaryUploadedFile
from django.core.paginator import InvalidPage
from django.db.models import IntegerField, Subquery
from django.utils import six, timezone
from PIL import Image
from rest_framework import serializers, status
from rest_framework.pagination import (LimitOffsetPagination,
                                       PageNumberPagination)
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from storages.backends.s3boto3 import S3Boto3Storage
from api.choices import UserType, IncidentStatus
from django.db.models import Q


ALL_SECURITY_ROLES = [x.value[0] for x in [UserType.security_admin, UserType.security_manager, UserType.security_staff]]

HOSPITALITY_ADMIN_ACCESSIBLES = [x.value[0] for x in [UserType.venue_admin, UserType.venue_manager, UserType.venue_staff]]
VENUE_ADMIN_ACCESSIBLES = [x.value[0] for x in [UserType.venue_admin, UserType.venue_manager, UserType.venue_staff]]
VENUE_MANAGER_ACCESSIBLES = [x.value[0] for x in [UserType.venue_manager, UserType.venue_staff]]
SECURITY_ADMIN_ACCESSIBLES = ALL_SECURITY_ROLES
SECURITY_MANAGER_ACCESSIBLES = [x.value[0] for x in [UserType.security_manager, UserType.security_staff]]


class CustomS3Boto3Storage(S3Boto3Storage):

    def _save_content(self, obj, content, parameters):
        """
        We create a clone of the content file as when this is passed to boto3 it wrongly closes
        the file upon upload where as the storage backend expects it to still be open
        """
        # Seek our content back to the start
        content.seek(0, os.SEEK_SET)

        # Create a temporary file that will write to disk after a specified size
        content_autoclose = SpooledTemporaryFile()

        # Write our original content into our copy that will be closed by boto3
        content_autoclose.write(content.read())

        # Upload the object which will auto close the content_autoclose instance
        super(
            CustomS3Boto3Storage,
            self)._save_content(
            obj,
            content_autoclose,
            parameters)

        # Cleanup if this is fixed upstream our duplicate should always close
        if not content_autoclose.closed:
            content_autoclose.close()


class StaticFileStorage(CustomS3Boto3Storage):
    querystring_auth = False


class CustomException(Exception):
    pass


class CapitalLetterValidator(object):
    HELP_TEXT = 'Password should contain at least one capital letter'

    def validate(self, password, user=None):
        if not any(str(x).isupper() for x in password):
            raise ValidationError(self.HELP_TEXT)

    def get_help_text(self):
        return self.HELP_TEXT


class NumericCharacterValidator(object):
    HELP_TEXT = 'Password should contain at least one number'

    def validate(self, password, user=None):
        if not any(x in string.digits for x in password):
            raise ValidationError(self.HELP_TEXT)

    def get_help_text(self):
        return self.HELP_TEXT


class SpecialCharacterValidator(object):
    HELP_TEXT = 'Password should contain at least one special character'

    def validate(self, password, user=None):
        if not any(x in string.punctuation for x in password):
            raise ValidationError(self.HELP_TEXT)

    def get_help_text(self):
        return self.HELP_TEXT


def custom_password_validator(password, user=None, password_validators=None):
    errors = []
    if ' ' in password:
        errors.append('Password cannot contain blank space')
    if password_validators is None:
        password_validators = get_default_password_validators()
    for validator in password_validators:
        try:
            validator.validate(password, user)
        except ValidationError as error:
            errors.extend(error.messages)

    if errors:
        if user:
            raise serializers.ValidationError(detail={'password': errors})
        else:
            raise serializers.ValidationError(detail=errors)


def validate_image(value):
    if hasattr(value, 'content_type'):
        if value.content_type.split('/')[0] != 'image':
            raise serializers.ValidationError(detail='Please upload a valid image file')
    if value.size > settings.MAX_UPLOAD_SIZE:
        raise serializers.ValidationError(
            detail='File size should be less than 10 Megabytes')


def get_image_from_b64(data):
    format, imgstr = data.split(';base64')
    random_name = ''.join(random.choice(string.ascii_letters) for _ in range(9))
    filename = random_name + '.png'
    image = ContentFile(base64.b64decode(imgstr), name=filename)
    return image


def get_dict_data(data):
    if isinstance(data, dict):
        return data
    else:
        return data.dict()


class CustomLimitOffsetPagination(LimitOffsetPagination):
    max_limit = 100


class CustomPagination(PageNumberPagination):
    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('total_pages', math.ceil(self.page.paginator.count / self.page_size)),
            ('data', data)
        ]))

    def paginate_queryset(self, queryset, request, view=None):
        """
        Paginate a queryset if required, either returning a
        page object, or `None` if pagination is not configured for this view.
        """
        page_size = self.get_page_size(request)
        if not page_size:
            return None

        paginator = self.django_paginator_class(queryset, page_size)
        page_number = request.query_params.get(self.page_query_param, 1)
        if page_number in self.last_page_strings:
            page_number = paginator.num_pages

        try:
            self.page = paginator.page(page_number)
        except InvalidPage as exc:
            msg = self.invalid_page_message.format(
                page_number=page_number, message=six.text_type(exc)
            )
            raise InvalidPage(msg)

        if paginator.num_pages > 1 and self.template is not None:
            # The browsable API should display pagination controls.
            self.display_page_controls = True

        self.request = request
        return list(self.page)


def paginated_success_response(data, message=None):
    return Response({'status': {'code': status.HTTP_200_OK,
                                'message': message},
                     **data})


def success_response(data=None, message=None, extra_data={}):
    result = {'status': {'code': status.HTTP_200_OK,
                         'message': message},
              'data': data
              }
    result.update(extra_data)
    return Response(result)


def error_response(data=None, message=None, code=status.HTTP_403_FORBIDDEN):
    return Response({'status': {'code': code,
                                'message': message},
                     'data': data
                     })


# def phonenumber_validator(value):
#     try:
#         z = phonenumbers.parse(value, None)
#         if not phonenumbers.is_valid_number(z):
#             raise ValidationError('Please enter a valid phone number')
#     except Exception as e:
#         if settings.DEBUG:
#             raise ValidationError(str(e))
#         else:
#             raise ValidationError('Please enter a valid phone number with international code')
#     return phonenumbers.format_number(z, phonenumbers.PhoneNumberFormat.E164)


# def disposable_mail_validator(value):
#     value = value.lower()
#     if not MailChecker.MailChecker.is_valid(value) and not settings.DEBUG:
#         raise ValidationError('Please enter a valid email')
#     return value


def get_jwt_auth_token(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


def random_file_name(extension, length):
    random_name = ''.join(random.choice(string.ascii_letters) for _ in range(length))
    return random_name + timezone.localdate().strftime('%Y%m%d') + extension


class SQCount(Subquery):
    template = "(SELECT count(*) FROM (%(subquery)s) _count)"
    output_field = IntegerField()

def filter_users_qs(qs, user, request):
    if request.query_params.get('venue') and (request.query_params.get('security_firm') or request.query_params.get('hospitality')) and request.method == 'GET':
        return qs
    elif user.is_superuser:
        return qs
    elif (user.user_type == UserType.hospitality_admin.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id, user_type__in=HOSPITALITY_ADMIN_ACCESSIBLES)
    elif (user.user_type == UserType.venue_admin.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id, user_type__in=VENUE_ADMIN_ACCESSIBLES)
    elif (user.user_type == UserType.venue_manager.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id, user_type__in=VENUE_MANAGER_ACCESSIBLES)
    elif (user.user_type == UserType.security_admin.value[0]):
        return qs.filter(security_firm_id=user.security_firm_id, user_type__in=SECURITY_ADMIN_ACCESSIBLES)
    elif (user.user_type == UserType.security_manager.value[0]):
        return qs.filter(security_firm_id=user.security_firm_id, user_type__in=SECURITY_MANAGER_ACCESSIBLES)


def filter_venue_qs(qs, user):
    if user.is_superuser:
        return qs
    elif (user.user_type == UserType.hospitality_admin.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id)
    elif (user.user_type == UserType.venue_admin.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id, venue_admins__id=user.id)
    elif (user.user_type == UserType.venue_manager.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id, venue_managers__id=user.id)
    elif (user.user_type == UserType.venue_staff.value[0]):
        return qs.filter(hospitality_id=user.hospitality_id, venue_staff__id=user.id)
    elif (user.user_type == UserType.security_admin.value[0]):
        return qs.filter(security_firms__id=user.security_firm_id)
    elif (user.user_type == UserType.security_manager.value[0]):
        return qs.filter(security_firms__id=user.security_firm_id)
    elif (user.user_type == UserType.security_staff.value[0]):
        return qs.filter(security_firms__id=user.security_firm_id, security_staff__id=user.id)


def get_user_venues_filter(user):
    if user.is_superuser:
        return Q()
    elif (user.user_type == UserType.hospitality_admin.value[0]):
        return Q(hospitality_id=user.hospitality_id)
    elif (user.user_type == UserType.venue_admin.value[0]):
        return Q(hospitality_id=user.hospitality_id, venue_admins__id=user.id)
    elif (user.user_type == UserType.venue_manager.value[0]):
        return Q(hospitality_id=user.hospitality_id, venue_managers__id=user.id)
    elif (user.user_type == UserType.venue_staff.value[0]):
        return Q(hospitality_id=user.hospitality_id, venue_staff__id=user.id)
    elif (user.user_type == UserType.security_admin.value[0]):
        return Q(security_firms__id=user.security_firm_id)
    elif (user.user_type == UserType.security_manager.value[0]):
        return Q(security_firms__id=user.security_firm_id)
    elif (user.user_type == UserType.security_staff.value[0]):
        return Q(security_firms__id=user.security_firm_id, security_staff__id=user.id)


def get_welcome_email_template_id(user_type, invited=False):
    if user_type == UserType.hospitality_admin.value[0]:
        return settings.WELCOME_HOSPITALITY_ADMIN_TEMPLATE
    if user_type == UserType.security_admin.value[0]:
        if invited:
            return settings.WELCOME_SECURITY_ADMIN_PASSWORD_TEMPLATE
        else:
            return settings.WELCOME_SECURITY_ADMIN_TEMPLATE
    elif user_type == UserType.security_manager.value[0]:
        return settings.WELCOME_SECURITY_MANAGER_TEMPLATE
    elif user_type == UserType.security_staff.value[0]:
        return settings.WELCOME_SECURITY_STAFF_TEMPLATE
    elif user_type == UserType.venue_admin.value[0]:
        return settings.WELCOME_VENUE_ADMIN_TEMPLATE
    elif user_type == UserType.venue_manager.value[0]:
        return settings.WELCOME_VENUE_MANAGER_TEMPLATE
    elif user_type == UserType.venue_staff.value[0]:
        return settings.WELCOME_VENUE_STAFF_TEMPLATE


def filter_incident_qs(qs, user, editable=False):
    if user.is_superuser:
        qs = qs.exclude(current_status=IncidentStatus.local_draft.value[0])
    elif (user.user_type == UserType.hospitality_admin.value[0]):
        qs = qs.filter(venue__hospitality_id=user.hospitality_id)
        if editable:
            qs = qs.filter(
                Q(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id) |
                Q(current_status__in=[x.value[0] for x in IncidentStatus if x != IncidentStatus.local_draft]))
    elif (user.user_type == UserType.venue_admin.value[0]):
        qs = qs.filter(venue__hospitality_id=user.hospitality_id, venue__venue_admins__id=user.id)
        if editable:
            qs = qs.filter(
                Q(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id) |
                Q(current_status__in=[x.value[0] for x in IncidentStatus if x != IncidentStatus.local_draft]))
    elif (user.user_type == UserType.venue_manager.value[0]):
        qs = qs.filter(
            venue__hospitality_id=user.hospitality_id,
            venue__venue_managers__id=user.id)
        if editable:
            qs = qs.filter(
                Q(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id) |
                Q(current_status=IncidentStatus.published_draft.value[0]))
    elif (user.user_type == UserType.venue_staff.value[0]):
        qs = qs.filter(venue__hospitality_id=str(user.hospitality_id), venue__venue_staff__id=str(user.id.hex))
        if editable:
            qs = qs.filter(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id)
    elif (user.user_type == UserType.security_admin.value[0]):
        qs = qs.filter(venue__security_firms__id=user.security_firm_id)
        if editable:
            qs = qs.filter(
                Q(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id) |
                Q(drafted_by__security_firm_id=user.security_firm_id))
    elif (user.user_type == UserType.security_manager.value[0]):
        qs = qs.filter(venue__security_firms__id=user.security_firm_id)
        if editable:
            qs = qs.filter(
                Q(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id) |
                Q(drafted_by__security_firm_id=user.security_firm_id, current_status=IncidentStatus.published_draft.value[0]))
    elif (user.user_type == UserType.security_staff.value[0]):
        qs = qs.filter(venue__security_firms__id=user.security_firm_id, venue__security_staff__id=user.id)
        if editable:
            qs = qs.filter(current_status=IncidentStatus.local_draft.value[0], drafted_by_id=user.id)
    return qs


def get_serializable_value(value):
    if type(value) == datetime.datetime:
        return str(value)
    elif type(value) == ImageFieldFile:
        return value.name
    else:
        return value


def get_changed_data(instance, new_data):
    old_data = {}
    new_data_dict = {}
    for x in new_data.keys():
        if getattr(instance, x) != new_data[x] and x != 'current_status':
            old_data[x] = get_serializable_value(getattr(instance, x))
            new_data_dict[x] = get_serializable_value(new_data[x])
    return old_data, new_data_dict


def filter_checklist_qs(qs, user):
    if user.is_superuser:
        return qs
    elif (user.user_type == UserType.hospitality_admin.value[0]):
        return qs.filter(venue__hospitality_id=user.hospitality_id)
    elif (user.user_type == UserType.venue_admin.value[0]):
        return qs.filter(venue__hospitality_id=user.hospitality_id, venue__venue_admins__id=user.id)
    elif (user.user_type == UserType.venue_manager.value[0]):
        return qs.filter(venue__hospitality_id=user.hospitality_id, venue__venue_managers__id=user.id)
    elif (user.user_type == UserType.venue_staff.value[0]):
        return qs.filter(venue__hospitality_id=user.hospitality_id, venue__venue_staff__id=user.id)
    elif (user.user_type == UserType.security_admin.value[0]):
        return qs.filter(venue__security_firms__id=user.security_firm_id)
    elif (user.user_type == UserType.security_manager.value[0]):
        return qs.filter(venue__security_firms__id=user.security_firm_id)
    elif (user.user_type == UserType.security_staff.value[0]):
        return qs.filter(venue__security_firms__id=user.security_firm_id, venue__security_staff__id=user.id)

def filter_checklist_qs_new(qs, user):
    return qs
