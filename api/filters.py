from api.models import CustomUser
from django_filters import rest_framework as filters
from api.choices import UserType
from django.db.models import Q
from api.models import Venue, Incident, CovidChecklist, ChecklistNew, ChecklistNewSubmited
import operator
from functools import reduce


class UserFilter(filters.FilterSet):
    user_type = filters.MultipleChoiceFilter(choices=[x.value for x in UserType])
    venue = filters.CharFilter(method='venue_related_users')
    not_venue = filters.CharFilter(method='not_venue_related_users')
    hospitality = filters.UUIDFilter(method='filter_multiple_ids')
    security_firm = filters.UUIDFilter(method='filter_multiple_ids')

    def filter_multiple_ids(self, queryset, key, value):
        if self.request.query_params.get('hospitality') and self.request.query_params.get('security_firm'):
            if key == 'security_firm':
                return queryset
            else:
                return queryset.filter(Q(hospitality_id__in=self.request.query_params.getlist('hospitality')) | Q(security_firm_id__in=self.request.query_params.getlist('security_firm')))
        else:
            filter_kwargs = {f'{key}_id__in': self.request.query_params.getlist(key)}
            return queryset.filter(**filter_kwargs)

    def venue_related_users(self, queryset, _, value):
        venue = Venue.objects.get(id=value)
        queryset = queryset.filter(
            Q(va_venues__id=value) |
            Q(vm_venues__id=value) |
            Q(vs_venues__id=value) |
            Q(ss_venues__id=value) |
            Q(Q(security_firm_id__in=venue.security_firms.values_list('id', flat=True)) & ~Q(user_type=UserType.security_staff.value[0]))
        )
        return queryset

    def not_venue_related_users(self, queryset, _, value):
        venue = Venue.objects.get(id=value)
        queryset = queryset.exclude(
            Q(va_venues__id=value) |
            Q(vm_venues__id=value) |
            Q(vs_venues__isnull=False) |
            Q(ss_venues__isnull=False) |
            Q(Q(security_firm_id__in=venue.security_firms.values_list('id', flat=True)) & ~Q(user_type=UserType.security_staff.value[0]))
        )
        return queryset

    class Meta:
        model = CustomUser
        fields = ('user_type',)


class IncidentFilter(filters.FilterSet):
    happened_from = filters.IsoDateTimeFilter(field_name='happened_at', lookup_expr='gte')
    happened_to = filters.IsoDateTimeFilter(field_name='happened_at', lookup_expr='lte')
    categories = filters.CharFilter(field_name='categories', method='filter_categories')
    locations = filters.CharFilter(method='filter_locations')
    related_user = filters.UUIDFilter(method='user_related_incidents')

    def user_related_incidents(self, queryset, key, value):
        user = CustomUser.objects.values('user_type', 'hospitality_id', 'security_firm_id').get(id=value)
        if user['user_type'] == UserType.hospitality_admin.value[0]:
            queryset = queryset.filter(venue__hospitality_id=user['hospitality_id'])
        elif user['user_type'] in [UserType.hospitality_admin.value[0],
                                           UserType.venue_admin.value[0], UserType.venue_manager.value[0], UserType.venue_staff.value[0]]:
            queryset = queryset.filter(
                Q(drafted_by__hospitality_id=user['hospitality_id'])|
                Q(submitted_by__hospitality_id=user['hospitality_id'])|
                Q(approved_by__hospitality_id=user['hospitality_id'])|
                Q(venue_manager__hospitality_id=user['hospitality_id'])
            )
        elif user['user_type'] == [UserType.security_admin.value[0], UserType.security_manager.value[0], UserType.security_staff.value[0]]:
            queryset = queryset.filter(
                Q(drafted_by__security_firm_id=user['security_firm_id'])|
                Q(submitted_by__security_firm_id=user['security_firm_id'])|
                Q(approved_by__security_firm_id=user['security_firm_id'])|
                Q(venue_manager__security_firm_id=user['security_firm_id'])
            )
        return queryset

    def filter_categories(self, queryset, key, value):
        categories = self.request.query_params.getlist(key)
        categories_filter = reduce(operator.__or__, [Q(categories__contains=[x]) for x in categories])
        return queryset.filter(categories_filter)

    def filter_locations(self, queryset, key, value):
        locations = self.request.query_params.getlist(key)
        return queryset.filter(location__in=locations)

    class Meta:
        model = Incident
        fields = ('drafted_by', 'actions', 'current_status', 'venue')


class ChecklistFilter(filters.FilterSet):
    created = filters.IsoDateTimeFromToRangeFilter(field_name='created_at')

    class Meta:
        model = CovidChecklist
        fields = ('venue', 'checklist_type')


class ChecklistFilterNew(filters.FilterSet):
    created = filters.IsoDateTimeFromToRangeFilter(field_name='created_at')

    class Meta:
        model = ChecklistNew
        fields = ('checklist_name','checklist_description')


class ChecklistFilterNewSubmited(filters.FilterSet):
    created = filters.IsoDateTimeFromToRangeFilter(field_name='created_at')

    class Meta:
        model = ChecklistNewSubmited
        fields = ('created_at','checklist_id')

