import uuid

from ckeditor.fields import RichTextField
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.postgres.fields import ArrayField, JSONField
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.postgres.fields import JSONField

from api.choices import (DeviceType, Gender, NotificationType,
                         SubscriptionUserType, UserType, IncidentStatus,
                         IncidentLogType, CovidChecklistType)
from api.upload_handlers import (handle_user_doc, handle_user_image,
                                 handle_venue_doc, handle_venue_logo, handle_incident_files,
                                 handle_checklist_files)
from djangosaml2idp.models import ServiceProvider


class BaseModel(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BaseOrganisation(BaseModel):
    name = models.CharField(max_length=300)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        abstract = True


class Subscription(BaseModel):
    plan_id = models.CharField(max_length=250)
    subscription_id = models.CharField(max_length=250, db_index=True)
    status = models.CharField(max_length=100, null=True)

    class Meta:
        abstract = True


class LegalTerm(BaseModel):
    '''To show on every login'''
    text = RichTextField()


class TermAndCondition(BaseModel):
    text = RichTextField()


class PaymentTerm(BaseModel):
    text = RichTextField()


class PrivacyPolicy(BaseModel):
    text = RichTextField()


class AdminContact(BaseModel):
    email = models.EmailField()
    phone_number = models.CharField(max_length=50)

    def __str__(self):
        return self.email


class SecurityFirm(BaseOrganisation):
    stripe_customer_id = models.TextField(null=True, blank=True)
    license_type = models.ForeignKey('LicenseType', on_delete=models.PROTECT, null=True)
    subscription_current_period_end = models.DateTimeField(null=True, blank=True)
    current_subscription_id = models.CharField(max_length=250, null=True, blank=True)
    # Possible values are incomplete, incomplete_expired, trialing, active,
    # past_due, canceled, or unpaid.
    current_subscription_status = models.CharField(max_length=100, null=True, blank=True)
    sso_service_provider = models.ForeignKey(
        ServiceProvider, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        ordering = ('name',)


class SecurityFirmSubscription(Subscription):
    security_firm = models.ForeignKey(SecurityFirm, on_delete=models.CASCADE)


class Hospitality(BaseOrganisation):
    stripe_customer_id = models.TextField(null=True, blank=True)
    sso_service_provider = models.ForeignKey(
        ServiceProvider, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        ordering = ('name',)


class CustomUserManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().select_related('security_firm', 'hospitality')

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        email = self.normalize_email(email).lower()
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class CustomUser(AbstractUser):
    """
    is_superuser tells if user is app admin
    for all the other users, user_type field is used.
    """
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    username = None
    user_type = models.CharField(max_length=20, choices=[x.value for x in UserType])
    email = models.EmailField(
        error_messages={"unique": "A user with this email already exists"},
        unique=True,
        db_index=True,
        max_length=150
    )
    notification_setting = models.BooleanField(default=True)
    unread_notification_count = models.PositiveIntegerField(default=0)
    profile_picture = models.ImageField(
        upload_to=handle_user_image, null=True, blank=True)
    security_firm = models.ForeignKey(
        SecurityFirm,
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    hospitality = models.ForeignKey(
        Hospitality,
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    address = models.CharField(max_length=500, null=True, blank=True)
    suburb = models.CharField(max_length=150, null=True, blank=True)
    state = models.CharField(max_length=150, null=True, blank=True)
    postcode = models.CharField(max_length=50, null=True, blank=True)
    phone_number = models.CharField(max_length=50, null=True)
    emergency_contact_name = models.CharField(max_length=100, null=True, blank=True)
    emergency_contact_phone = models.CharField(max_length=50, null=True, blank=True)
    # For all types of users. Competency cards in case of hospitality users
    # and security license in case of security user
    license_number = models.CharField(max_length=150, null=True, blank=True)
    # For all types of users. Competency cards in case of hospitality users
    # and security license in case of security user
    license_type = models.CharField(max_length=150, null=True, blank=True)
    # For all types of users. Competency cards in case of hospitality users
    # and security license in case of security user
    license_expiry = models.DateField(null=True, blank=True)
    is_license_verified = models.BooleanField(
        default=False,
        help_text='This tells that the license details were retrived from nsw gov API')
    is_license_active = models.BooleanField(
        default=False,
        help_text='This tells that the license details are from the NSW Gov API and it is not expired')
    sso_service_provider = models.ForeignKey(
        ServiceProvider, on_delete=models.CASCADE, null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    class Meta:
        ordering = ('first_name',)


class DeviceToken(BaseModel):
    device_type = models.CharField(max_length=10, choices=[x.value for x in DeviceType])
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    token = models.TextField()


class UserNotification(BaseModel):
    notification_type = models.CharField(
        max_length=20, choices=[
            x.value for x in NotificationType])
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    body = models.CharField(max_length=500)
    id_data = models.CharField(max_length=50, null=True)
    is_read = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created_at',)


class UserDocument(BaseModel):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    document = models.FileField(upload_to=handle_user_doc)

    class Meta:
        ordering = ('-created_at',)


class LicenseType(BaseModel):
    name = models.CharField(max_length=100, unique=True)
    amount = models.DecimalField(max_digits=6, decimal_places=2, null=True)
    subscription_plan_id = models.CharField(max_length=200, unique=True)
    subscription_type = models.CharField(
        max_length=2, choices=[
            x.value for x in SubscriptionUserType])
    description = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('amount',)


class Venue(BaseModel):
    # created_by - hospitality admin of the related hospitality
    hospitality = models.ForeignKey(Hospitality, on_delete=models.CASCADE)
    logo = models.ImageField(upload_to=handle_venue_logo, null=True, blank=True)
    venue_admins = models.ManyToManyField(CustomUser, related_name='va_venues')
    venue_managers = models.ManyToManyField(CustomUser, related_name='vm_venues')
    venue_staff = models.ManyToManyField(CustomUser, related_name='vs_venues')
    security_staff = models.ManyToManyField(CustomUser, related_name='ss_venues')
    name = models.CharField(
        max_length=250,
        null=True,
        unique=True,
        error_messages={"unique": "A Venue with this name exists"},)
    address = models.CharField(max_length=500, null=True)
    suburb = models.CharField(max_length=150, null=True)
    state = models.CharField(max_length=150, null=True)
    postcode = models.CharField(max_length=50, null=True)
    website = models.URLField(null=True)
    abn = models.CharField(
        max_length=11,
        validators=[
            RegexValidator(r'^\d{11}$')],
        null=True)
    license_type = models.ForeignKey(LicenseType, on_delete=models.PROTECT, null=True)
    license_number = models.CharField(max_length=150, null=True, blank=True)
    license_validity_date = models.DateField(null=True, blank=True)
    phone = models.CharField(max_length=50, null=True)
    email = models.EmailField(null=True, blank=True)
    avg_patronage = models.PositiveIntegerField(null=True, blank=True)
    precint = models.CharField(max_length=250, null=True, blank=True)
    trading_hours = JSONField(default=dict)
    bar_locations = ArrayField(
        models.CharField(max_length=50),
        default=list,
        help_text='Use "," to separate the elements',
    )
    categories = ArrayField(
        models.CharField(max_length=50),
        default=list,
        help_text='Use "," to separate the elements',
    )
    security_firms = models.ManyToManyField(SecurityFirm)
    current_subscription_id = models.CharField(max_length=250, null=True, blank=True)
    subscription_current_period_end = models.DateTimeField(null=True, blank=True)
    # Possible values are incomplete, incomplete_expired, trialing, active,
    # past_due, canceled, or unpaid.
    current_subscription_status = models.CharField(max_length=100, null=True, blank=True)
    prefix_id = models.CharField(max_length=20, null=True)
    latest_incident_id = models.PositiveIntegerField(default=0)
    sso_service_provider = models.ForeignKey(
        ServiceProvider, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        ordering = ('-created_at',)


class VenueDocument(BaseModel):
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE, null=True)
    document = models.FileField(upload_to=handle_venue_doc)

    class Meta:
        ordering = ('-created_at',)


class VenueSubscription(Subscription):
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE, null=True)


class SubscriptionPayment(BaseModel):
    """Common model to store payments from security firm subscriptions and venue subscriptions"""
    subscription_id = models.CharField(max_length=250, null=True, blank=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2)


class IncidentAction(BaseModel):
    title = models.CharField(
        max_length=50,
        unique=True,
        error_messages={'unique': 'A matching action already exists'})

    class Meta:
        ordering = ('title',)


class Incident(BaseModel):
    unique_id = models.CharField(max_length=20, null=True, blank=True)
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE)
    venue_manager = models.ForeignKey(
        CustomUser,
        on_delete=models.PROTECT,
        related_name='managed_incidents',
        null=True,
        blank=True)
    current_status = models.IntegerField(choices=[x.value for x in IncidentStatus])
    drafted_by = models.ForeignKey(
        CustomUser,
        on_delete=models.PROTECT,
        related_name='drafted_incidents',
        null=True,
        blank=True)
    submitted_by = models.ForeignKey(
        CustomUser,
        on_delete=models.PROTECT,
        related_name='submitted_incidents',
        null=True,
        blank=True)
    approved_by = models.ForeignKey(
        CustomUser,
        on_delete=models.PROTECT,
        related_name='approved_incidents',
        null=True,
        blank=True)
    drafted_at = models.DateTimeField(null=True, blank=True)
    submitted_at = models.DateTimeField(null=True, blank=True)
    approved_at = models.DateTimeField(null=True, blank=True)
    happened_at = models.DateTimeField(null=True, blank=True)
    location = models.CharField(max_length=50, null=True, blank=True)
    categories = ArrayField(
        models.CharField(max_length=50),
        default=list,
        help_text='Use "," to separate the elements',
    )
    actions = models.ManyToManyField(IncidentAction)
    person_count = models.PositiveIntegerField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    footage_on_cctv = models.BooleanField(default=False)
    footage_on_disc = models.BooleanField(default=False)
    linked_incident = models.ForeignKey(
        'self', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        ordering = ('-modified_at',)


class IncidentPerson(BaseModel):
    incident = models.ForeignKey(
        Incident,
        on_delete=models.CASCADE,
        related_name='incident_persons')
    name = models.CharField(max_length=150)
    age = models.PositiveIntegerField()
    email = models.EmailField()
    gender = models.CharField(max_length=1, choices=[x.value for x in Gender])
    license_number = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=50)
    image = models.ImageField(upload_to=handle_incident_files, null=True, blank=True)
    is_witness = models.BooleanField()
    build = models.CharField(max_length=200, null=True, blank=True)
    height = models.CharField(max_length=100, null=True, blank=True)
    hair = models.CharField(max_length=100, null=True, blank=True)
    top_clothing = models.CharField(max_length=150, null=True, blank=True)
    bottom_clothing = models.CharField(max_length=150, null=True, blank=True)
    shoes = models.CharField(max_length=150, null=True, blank=True)
    other_description = models.CharField(max_length=200, null=True, blank=True)
    weapon = models.CharField(max_length=200, null=True, blank=True)
    injury_description = models.CharField(max_length=250, null=True, blank=True)
    injury_image = models.ImageField(
        upload_to=handle_incident_files, null=True, blank=True)

    class Meta:
        ordering = ('created_at',)


class IncidentMedia(BaseModel):
    incident = models.ForeignKey(Incident, on_delete=models.CASCADE, null=True)
    media_file = models.FileField(upload_to=handle_incident_files)

    class Meta:
        ordering = ('-created_at',)


class IncidentLog(BaseModel):
    log_type = models.PositiveIntegerField(choices=[x.value for x in IncidentLogType])
    incident = models.ForeignKey(Incident, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(CustomUser, on_delete=models.PROTECT)
    old_data = JSONField(null=True)
    updated_data = JSONField(null=True)

    class Meta:
        ordering = ('-created_at',)


class NSWGovAPIToken(models.Model):
    token = models.CharField(max_length=200)
    expires_at = models.DateTimeField()

    class Meta:
        ordering = ('-expires_at',)


class SecurityLicense(models.Model):
    license_number = models.CharField(max_length=150, primary_key=True)
    expiry_date = models.DateField()
    license_type = models.CharField(max_length=150)


class CovidChecklist(BaseModel):
    checklist_type = models.IntegerField(choices=[x.value for x in CovidChecklistType])
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    venue = models.ForeignKey(Venue, on_delete=models.CASCADE)
    checklist_json = JSONField(default=dict)
    attachments_count = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('-created_at',)


class ChecklistImage(BaseModel):
    checklist = models.ForeignKey(CovidChecklist, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=handle_checklist_files)

    class Meta:
        ordering = ('-created_at',)


class ChecklistNew(BaseModel):
    checklist_name=models.CharField(max_length=150)
    checklist_description=models.CharField(max_length=150)
    #user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    #https://docs.djangoproject.com/en/3.1/topics/db/examples/many_to_many/
    users= models.ManyToManyField(CustomUser)
    checklist_json = JSONField(default=dict)
    checklist_recurring=models.CharField(max_length=150, blank=True)
    recuring_mon=models.CharField(max_length=150, blank=True)
    recuring_tue =models.CharField(max_length=150, blank=True)
    recuring_wed =models.CharField(max_length=150, blank=True)
    recuring_fri =models.CharField(max_length=150, blank=True, null=True,default=None)
    recuring_sat=models.CharField(max_length=150, blank=True)
    recuring_sun =models.CharField(max_length=150, blank=True)
    recuring_type=models.CharField(max_length=150, blank=True)
    recuring_start=models.DateField(blank=True)
    recuring_start_time=models.TimeField(blank=True)
    recuring_end_time=models.TimeField(blank=True)
    recuring_reminder =models.CharField(max_length=150, blank=True)
    class Meta:
        ordering = ('-created_at',)


class ChecklistNewSubmited(BaseModel):
    checklist=models.ForeignKey(ChecklistNew, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    checklist_json = JSONField(default=dict)
    class Meta:
        ordering = ('-created_at',)
