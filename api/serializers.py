import uuid

from django.contrib.auth.hashers import make_password
from django.db.models import Sum
from django.http import QueryDict
from rest_framework import serializers
from django.utils import timezone

from api.choices import IncidentStatus, UserType
from api.models import (AdminContact, CustomUser, DeviceToken, Hospitality,
                        Incident, IncidentAction, IncidentLog, IncidentMedia,
                        IncidentPerson, LicenseType, SecurityFirm,
                        SecurityFirmSubscription, SubscriptionPayment,
                        UserDocument, UserNotification, Venue, VenueDocument,
                        VenueSubscription, SecurityLicense, CovidChecklist, ChecklistImage,ChecklistNew, ChecklistNewSubmited)
from api.utils import (HOSPITALITY_ADMIN_ACCESSIBLES,
                       SECURITY_ADMIN_ACCESSIBLES,
                       SECURITY_MANAGER_ACCESSIBLES, VENUE_ADMIN_ACCESSIBLES,
                       VENUE_MANAGER_ACCESSIBLES, get_image_from_b64,
                       CustomException, filter_venue_qs)
from djangosaml2idp.models import ServiceProvider
from dateutil.relativedelta import relativedelta


def validate_license_data(validated_data, instance_id=None):
    if validated_data.get('license_number'):
        if CustomUser.objects.filter(
                license_number__iexact=validated_data['license_number']).exclude(id=instance_id).exists():
            raise CustomException('This license number has been already registered.')
        valid_license_object = SecurityLicense.objects.filter(
            license_number=validated_data['license_number']).first()
        if valid_license_object:
            validated_data['license_type'] = valid_license_object.license_type
            validated_data['license_expiry'] = valid_license_object.expiry_date
            validated_data['is_license_active'] = valid_license_object.expiry_date > timezone.localdate(
            )
            validated_data['is_license_verified'] = True
        else:
            # validated_data['license_type'] = None
            # validated_data['license_expiry'] = valid_license_object.expiry_date
            validated_data['is_license_active'] = False
            validated_data['is_license_verified'] = False
    return validated_data


class ServiceProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceProvider
        fields = ('name', 'login_url', 'entity')


class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str):
            if data == 'null':
                return None
            # if (len(data) * 6) > (1024 * 1024):
            #     raise CustomException('Image size cannot be more than 1MB')
            data = get_image_from_b64(data.replace(' ', '+'))
        return super().to_internal_value(data)


class CustomListField(serializers.ListField):
    def to_internal_value(self, data):
        if isinstance(self.context["request"].data, QueryDict):
            data = data[0]
        return super().to_internal_value(data)


class TextSerializer(serializers.Serializer):
    text = serializers.CharField()
    modified_on = serializers.DateTimeField(read_only=True)

    def create(self, validated_data):
        instance = self.context["model"](**validated_data)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        instance.text = validated_data["text"]
        instance.save()
        return instance


class AdminContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdminContact
        fields = ('email', 'phone_number')


class DeviceTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceToken
        fields = ('device_type', 'token')

    def create(self, validated_data):
        existing_token = DeviceToken.objects.filter(
            device_type=validated_data['device_type'], user_id=validated_data['user'].id).first()
        if existing_token:
            return self.update(existing_token, validated_data)
        return super().create(validated_data)


class UserNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserNotification
        fields = (
            'id',
            'created_at',
            'notification_type',
            'title',
            'body',
            'is_read',
            'id_data')
        read_only_fields = ('notification_type', 'title', 'body', 'id_data')


class ShortUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            'id',
            'is_active',
            'first_name',
            'last_name',
            'email',
            'profile_picture',
            'user_type',
            'security_firm_id',
            'hospitality_id',
            'license_number',
            'license_type',
            'license_expiry',
            'is_license_verified',
            'is_license_active',)


class SecurityFirmSerializer(serializers.ModelSerializer):
    class Meta:
        model = SecurityFirm
        fields = (
            'id',
            'name',
            'email',
            'phone',
            'subscription_current_period_end',
            'license_type_id')

    def to_representation(self, value):
        data = super().to_representation(value)
        data['is_active_subscription'] = value.current_subscription_status == 'active'
        if self.context.get('detail_view'):
            subscription_ids = SecurityFirmSubscription.objects.filter(
                security_firm_id=value.id
            ).values_list('subscription_id', flat=True)
            data['revenue'] = SubscriptionPayment.objects.filter(
                subscription_id__in=subscription_ids).aggregate(
                Sum('amount'))['amount__sum']
        if self.context.get('service_provider'):
            data['sso_service_provider'] = ServiceProviderSerializer(
                value.sso_service_provider, context=self.context).data
        return data


class HospitalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Hospitality
        fields = ('id', 'name', 'email', 'phone')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if self.context.get('service_provider'):
            data['sso_service_provider'] = ServiceProviderSerializer(
                instance.sso_service_provider, context=self.context).data
        return data


class UserBaseSerializer(serializers.ModelSerializer):
    def validate_email(self, value):
        if CustomUser.objects.filter(email__iexact=value).exists():
            raise serializers.ValidationError('A user with this email already exists')
        return value.lower()

    def to_representation(self, value):
        data = super().to_representation(value)
        if value.security_firm_id:
            data['security_firm'] = SecurityFirmSerializer(
                value.security_firm, context=self.context).data
        if value.hospitality_id:
            data['hospitality'] = HospitalitySerializer(
                value.hospitality, context=self.context).data
        return data


class RegistrationSerializer(UserBaseSerializer):
    notification_setting = serializers.BooleanField(default=True)
    user_type = serializers.ChoiceField(
        choices=[x.value[0] for x in [UserType.hospitality_admin, UserType.security_admin]])
    profile_picture = Base64ImageField(required=False)

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'password',
            'date_joined',
            'first_name',
            'last_name',
            'email',
            'user_type',
            'notification_setting',
            'unread_notification_count',
            'is_superuser',
            'profile_picture',
            'phone_number',
            'license_number',
            'license_type',
            'license_expiry',
            'is_license_verified',
            'is_license_active',
            'address',
            'suburb',
            'state',
            'postcode',
            'emergency_contact_phone',
            'emergency_contact_name',
            'profile_picture',
        )
        read_only_fields = (
            'is_license_verified',
            'is_license_active',
            'license_type',
            'license_expiry',)

    def __init__(self, *args, **kwargs):
        if kwargs.get('data'):
            if isinstance(kwargs['data'], QueryDict):
                kwargs['data'] = kwargs['data'].dict()
            email_data = kwargs['data'].get('email')
            if email_data:
                kwargs['data']['email'] = email_data.lower()
        super().__init__(*args, **kwargs)

    def validate_password(self, value):
        return make_password(value)

    def create(self, validated_data):
        validated_data = validate_license_data(validated_data)
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data.pop('password', None)
        data['is_active'] = instance.is_active
        venue = None
        if instance.user_type == UserType.venue_staff.value[0]:
            venue = Venue.objects.select_related('license_type').prefetch_related(
                'security_firms').filter(venue_staff__id=instance.id).first()
        if instance.user_type == UserType.security_staff.value[0]:
            venue = Venue.objects.select_related('license_type').prefetch_related(
                'security_firms').filter(security_staff__id=instance.id).first()
        data['venue_details'] = VenueSerializer(
            venue, context=self.context).data if venue else None
        return data


class UserPasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ("password",)

    def update(self, instance, validated_data):
        validated_data["password"] = make_password(validated_data["password"])
        return super().update(instance, validated_data)


class ProfileUpdateSerializer(UserBaseSerializer):
    profile_picture = Base64ImageField(required=False)
    sso_service_provider = ServiceProviderSerializer(read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'first_name',
            'last_name',
            'email',
            'user_type',
            'notification_setting',
            'unread_notification_count',
            'is_superuser',
            'profile_picture',
            'address',
            'suburb',
            'state',
            'postcode',
            'phone_number',
            'emergency_contact_name',
            'emergency_contact_phone',
            'license_number',
            'license_type',
            'license_expiry',
            'is_license_verified',
            'is_license_active',
            'sso_service_provider',
            'date_joined')
        read_only_fields = (
            'user_type',
            'is_superuser',
            'is_license_verified',
            'is_license_active',
        )

    def update(self, instance, validated_data):
        validated_data = validate_license_data(validated_data, instance_id=instance.id)
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        venue = None
        if instance.user_type == UserType.venue_staff.value[0]:
            venue = Venue.objects.select_related('license_type').prefetch_related(
                'security_firms').filter(venue_staff__id=instance.id).first()
        if instance.user_type == UserType.security_staff.value[0]:
            venue = Venue.objects.select_related('license_type').prefetch_related(
                'security_firms').filter(security_staff__id=instance.id).first()
        data['venue_details'] = VenueSerializer(
            venue, context=self.context).data if venue else None
        return data


class UserSerializer(UserBaseSerializer):
    profile_picture = Base64ImageField(required=False)
    notification_setting = serializers.BooleanField(default=True)

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'is_active',
            'date_joined',
            'first_name',
            'last_name',
            'email',
            'user_type',
            'notification_setting',
            'unread_notification_count',
            'is_superuser',
            'last_login',
            'profile_picture',
            'address',
            'suburb',
            'state',
            'postcode',
            'phone_number',
            'emergency_contact_name',
            'emergency_contact_phone',
            'license_number',
            'license_type',
            'license_expiry',
            'is_license_verified',
            'is_license_active',
        )
        read_only_fields = (
            'date_joined',
            'is_superuser',
            'last_login',
            'is_license_verified',
            'is_license_active',
        )

    def create(self, validated_data):
        validated_data = validate_license_data(validated_data)
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data = validate_license_data(validated_data, instance_id=instance.id)
        return super().update(instance, validated_data)

    def validate_user_type(self, value):
        user = self.context['request'].user
        valid_user_types = []
        if user.user_type == UserType.hospitality_admin.value[0]:
            valid_user_types = HOSPITALITY_ADMIN_ACCESSIBLES
        elif user.user_type == UserType.venue_admin.value[0]:
            valid_user_types = VENUE_ADMIN_ACCESSIBLES
        elif user.user_type == UserType.venue_manager.value[0]:
            valid_user_types = VENUE_MANAGER_ACCESSIBLES
        elif user.user_type == UserType.security_admin.value[0]:
            valid_user_types = SECURITY_ADMIN_ACCESSIBLES
        elif user.user_type == UserType.security_manager.value[0]:
            valid_user_types = SECURITY_MANAGER_ACCESSIBLES
        if value not in valid_user_types:
            raise serializers.ValidationError('Invalid user type')
        return value

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if self.context.get('venues'):
            if instance.user_type == UserType.venue_admin.value[0]:
                data['venues'] = VenueListSerializer(
                    instance.va_venues.all(), many=True, context=self.context).data
            if instance.user_type == UserType.venue_manager.value[0]:
                data['venues'] = VenueListSerializer(
                    instance.vm_venues.all(), many=True, context=self.context).data
            if instance.user_type == UserType.venue_staff.value[0]:
                data['venues'] = VenueListSerializer(
                    instance.vs_venues.all(), many=True, context=self.context).data
            if instance.user_type == UserType.security_staff.value[0]:
                data['venues'] = VenueListSerializer(
                    instance.ss_venues.all(), many=True, context=self.context).data
        venue = None
        if instance.user_type == UserType.venue_staff.value[0]:
            venue = Venue.objects.select_related('license_type').prefetch_related(
                'security_firms').filter(venue_staff__id=instance.id).first()
        if instance.user_type == UserType.security_staff.value[0]:
            venue = Venue.objects.select_related('license_type').prefetch_related(
                'security_firms').filter(security_staff__id=instance.id).first()
        data['venue_details'] = VenueSerializer(
            venue, context=self.context).data if venue else None
        if instance.user_type == UserType.hospitality_admin.value[0]:
            subscription_ids = VenueSubscription.objects.filter(
                venue__hospitality_id=instance.hospitality_id
            ).values_list('subscription_id', flat=True)
            data['revenue'] = SubscriptionPayment.objects.filter(
                subscription_id__in=subscription_ids).aggregate(
                Sum('amount'))['amount__sum']
        if self.context.get('sso_data') and instance.sso_service_provider_id:
            data['sso_service_provider'] = ServiceProviderSerializer(
                instance.sso_service_provider, context=self.context).data
        return data


class StripeSourceSerializer(serializers.Serializer):
    brand = serializers.CharField()
    country = serializers.CharField()
    exp_month = serializers.CharField()
    exp_year = serializers.CharField()
    id = serializers.CharField()
    last4 = serializers.CharField()
    source_type = serializers.CharField(source="object")


class UserDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDocument
        fields = ('id', 'document')


class LicenseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LicenseType
        fields = (
            'id',
            'name',
            'amount',
            'subscription_plan_id',
            'subscription_type',
            'description')

    def to_representation(self, value):
        data = super().to_representation(value)
        if not self.context['request'].user.is_superuser:
            data.pop('subscription_plan_id', None)
        return data


class VenueListSerializer(serializers.ModelSerializer):
    categories = CustomListField(child=serializers.CharField(max_length=50))
    bar_locations = CustomListField(child=serializers.CharField(max_length=50))

    class Meta:
        model = Venue
        fields = (
            'id',
            'logo',
            'created_at',
            'name',
            'address',
            'suburb',
            'state',
            'postcode',
            'website',
            'abn',
            'categories',
            'bar_locations',
            'subscription_current_period_end',
        )

    def to_representation(self, value):
        data = super().to_representation(value)
        data['is_active_subscription'] = value.current_subscription_status == 'active'
        if self.context.get('service_provider'):
            data['sso_service_provider'] = ServiceProviderSerializer(
                value.sso_service_provider, context=self.context).data
        return data


class VenueSerializer(serializers.ModelSerializer):
    security_firms = SecurityFirmSerializer(many=True, read_only=True)
    bar_locations = CustomListField(child=serializers.CharField(max_length=50))
    categories = CustomListField(child=serializers.CharField(max_length=50))
    license_type = LicenseTypeSerializer(read_only=True)
    logo = Base64ImageField(required=False)

    class Meta:
        model = Venue
        fields = (
            'id',
            'logo',
            'created_at',
            'name',
            'address',
            'suburb',
            'state',
            'postcode',
            'website',
            'abn',
            'license_type',
            'license_number',
            'license_validity_date',
            'phone',
            'email',
            'avg_patronage',
            'precint',
            'trading_hours',
            'bar_locations',
            'categories',
            'security_firms',
            'subscription_current_period_end',
        )

    def create(self, validated_data):
        instance = super().create(validated_data)
        # HACK: We're saving subscription details in the returning venue object
        # as stripe webhook updates the venue subscription status and when the
        # venue object is returned, it does not show any subscription detail.
        instance.current_subscription_status = 'active'
        instance.subscription_current_period_end = timezone.now() + relativedelta(months=1)
        return instance

    def to_representation(self, value):
        data = super().to_representation(value)
        data['is_active_subscription'] = value.current_subscription_status == 'active'
        return data


class VenueDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = VenueDocument
        fields = ('id', 'document')


class IncidentActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = IncidentAction
        fields = ('id', 'title')


class IncidentPersonSerializer(serializers.ModelSerializer):
    injury_image = Base64ImageField(required=False, allow_null=True)

    class Meta:
        model = IncidentPerson
        fields = (
            'id',
            'created_at',
            'name',
            'age',
            'email',
            'gender',
            'license_number',
            'phone_number',
            'image',
            'is_witness',
            'build',
            'height',
            'hair',
            'top_clothing',
            'bottom_clothing',
            'shoes',
            'other_description',
            'weapon',
            'injury_description',
            'injury_image',
        )

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if self.context.get('no_image'):
            data['image'] = instance.image.name if instance.image else None
        return data


class IncidentListSerializer(serializers.ModelSerializer):
    venue = VenueListSerializer(read_only=True)
    drafted_by = ShortUserSerializer(read_only=True)
    categories = CustomListField(child=serializers.CharField(max_length=50))
    actions = IncidentActionSerializer(many=True, read_only=True)

    class Meta:
        model = Incident
        fields = ('id', 'created_at', 'modified_at', 'venue', 'current_status', 'drafted_by',
                  'happened_at', 'location', 'categories', 'actions',
                  'summary', 'unique_id')
        read_only_fields = ('unique_id',)


class IncidentSerializer(serializers.ModelSerializer):
    venue = VenueListSerializer(read_only=True)
    venue_manager = ShortUserSerializer(read_only=True)
    drafted_by = ShortUserSerializer(read_only=True)
    submitted_by = ShortUserSerializer(read_only=True)
    approved_by = ShortUserSerializer(read_only=True)
    categories = CustomListField(child=serializers.CharField(max_length=50))
    actions = IncidentActionSerializer(many=True, read_only=True)
    linked_incident = IncidentListSerializer(read_only=True)

    class Meta:
        model = Incident
        fields = ('id', 'created_at', 'modified_at', 'venue', 'venue_manager', 'current_status', 'drafted_by',
                  'submitted_by', 'approved_by', 'happened_at', 'location', 'categories', 'actions', 'person_count',
                  'summary', 'footage_on_cctv', 'footage_on_disc', 'linked_incident', 'unique_id'
                  )
        read_only_fields = ('unique_id',)

    def validate_current_status(self, value):
        user = self.context['request'].user
        valid_status = []
        if user.user_type in [UserType.hospitality_admin.value[0],
                              UserType.venue_admin.value[0], UserType.security_admin.value[0]]:
            valid_status = [x.value[0] for x in IncidentStatus]
        elif user.user_type in [UserType.venue_manager.value[0], UserType.security_manager.value[0]]:
            valid_status = [
                IncidentStatus.local_draft.value[0],
                IncidentStatus.published_draft.value[0],
                IncidentStatus.submitted.value[0]]
        elif user.user_type in [UserType.venue_staff.value[0], UserType.security_staff.value[0]]:
            valid_status = [
                IncidentStatus.local_draft.value[0],
                IncidentStatus.published_draft.value[0]]
        if value not in valid_status:
            raise serializers.ValidationError('Invalid current status')
        return value
    
    def to_representation(self, instance):
        data = super().to_representation(instance)
        dt = serializers.DateTimeField()
        data['modification_timestamps'] = [dt.to_representation(x.created_at) for x in instance.incidentlog_set.all()]
        return data


class IncidentMediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = IncidentMedia
        fields = ('id', 'media_file')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if self.context.get('no_image'):
            data['media_file'] = instance.media_file.name if instance.media_file else None
        return data


class IncidentLogSerializer(serializers.ModelSerializer):
    user = ShortUserSerializer(read_only=True)

    class Meta:
        model = IncidentLog
        fields = ('id', 'created_at', 'log_type', 'user', 'old_data', 'updated_data')


class CovidChecklistShortSerializer(serializers.ModelSerializer):
    user = ShortUserSerializer(read_only=True)
    venue = VenueListSerializer(read_only=True)

    class Meta:
        model = CovidChecklist
        fields = (
            'id',
            'created_at',
            'checklist_type',
            'user',
            'venue',
            'attachments_count',
        )


class CovidChecklistSerializer(serializers.ModelSerializer):
    user = ShortUserSerializer(read_only=True)
    venue = VenueListSerializer(read_only=True)
    checklist_json = serializers.JSONField(required=True, allow_null=False)
    attachments_count = serializers.IntegerField(required=True, allow_null=False)

    class Meta:
        model = CovidChecklist
        fields = (
            'id',
            'created_at',
            'checklist_type',
            'user',
            'venue',
            'checklist_json',
            'attachments_count',
        )
    
    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        validated_data['venue'] = filter_venue_qs(Venue.objects.all(), self.context['request'].user).get(id=self.initial_data['venue'])
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        image_serializer_field = serializers.ImageField()
        data['images'] = [x.image.url for x in instance.checklistimage_set.only('image')]
        return data

class CovidChecklistSerializerNew(serializers.ModelSerializer):
    checklist_json = serializers.JSONField(required=True, allow_null=False)

    class Meta:
        model = ChecklistNew
        fields = (
            'id',
            'created_at',
            'checklist_json',
            'checklist_name',
            'checklist_description'

        )


    def create(self, validated_data):
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data


class CovidChecklistSerializerNewSubmited(serializers.ModelSerializer):
    user = ShortUserSerializer(read_only=True)
    checklist_json = serializers.JSONField(required=True, allow_null=False)

    class Meta:
        model = ChecklistNewSubmited
        fields = (
            'id',
            'created_at',
            'user',
            'checklist_json',
            'checklist_id',
        )

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data


class ChecklistImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChecklistImage
        fields = (
            'image',
        )
    
    def create(self, validated_data):
        validated_data['checklist_id'] = uuid.UUID(self.initial_data['checklist'])
        return super().create(validated_data)
