import io
import json
import logging
import secrets
import uuid
import os

import requests
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import check_password, make_password
from django.db import transaction
from django.db import connection
from django.db.models import F, Prefetch, Q, Sum
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.utils import timezone
from django.views import View
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.authentication import AUTH_HEADER_TYPES
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.serializers import TokenRefreshSerializer

from api.base_permissions import (CanAccessUsers, CanAssignSecurityStaff,
								  CanAssignVenueStaff, CanUpdateVenue,
								  CanViewVenueDoc, IsHospitalityAdmin,
								  IsSecurityAdmin, IsSuperUser)
from api.base_views import *
from api.choices import (IncidentLogType, IncidentStatus, NotificationType,
						 SubscriptionUserType, UserType)
from api.filters import IncidentFilter, UserFilter, ChecklistFilter,ChecklistFilterNew, ChecklistFilterNewSubmited
from api.models import (AdminContact, CustomUser, DeviceToken, Hospitality,
						Incident, IncidentAction, IncidentLog, IncidentMedia,
						IncidentPerson, LegalTerm, LicenseType, NSWGovAPIToken,
						PaymentTerm, PrivacyPolicy, SecurityFirm,
						SecurityFirmSubscription, SubscriptionPayment,
						TermAndCondition, UserDocument, UserNotification,
						Venue, VenueDocument, VenueSubscription, CovidChecklist, ChecklistImage, ChecklistNew,ChecklistNewSubmited )
from api.payments import (cancel_subscription, create_subscription,
						  get_stripe_customer)
from api.query_utils import validate_security_license
from api.serializers import (AdminContactSerializer, DeviceTokenSerializer,
							 HospitalitySerializer, IncidentActionSerializer,
							 IncidentListSerializer, IncidentLogSerializer,
							 IncidentMediaSerializer, IncidentPersonSerializer,
							 IncidentSerializer, LicenseTypeSerializer,
							 ProfileUpdateSerializer, RegistrationSerializer,
							 SecurityFirmSerializer, ServiceProviderSerializer,
							 TextSerializer, UserDocumentSerializer,
							 UserNotificationSerializer,
							 UserPasswordSerializer, UserSerializer,
							 VenueDocumentSerializer, VenueListSerializer,
							 VenueSerializer, CovidChecklistShortSerializer,
							 CovidChecklistSerializer, ChecklistImageSerializer, CovidChecklistSerializerNew, CovidChecklistSerializerNewSubmited)
from api.tasks import (bucket, create_incident_detail_report,
					   create_incident_report, send_admin_error_alert,
					   send_batch_mail, send_notification_to_users,
					   send_sendgrid_mail, create_checklist_report)
from api.utils import (ALL_SECURITY_ROLES, CustomException, CustomPagination,
					   error_response, filter_incident_qs, filter_users_qs,
					   filter_venue_qs, get_changed_data, get_jwt_auth_token,
					   get_user_venues_filter, get_welcome_email_template_id,
					   paginated_success_response, success_response, filter_checklist_qs,filter_checklist_qs_new)
from djangosaml2idp.models import ServiceProvider
from strings import *
from rest_framework import mixins
from django.forms.models import model_to_dict
from django.core.paginator import Paginator
import datetime
logger = logging.getLogger(__name__)


class TermAndConditionAPI(CustomAPIView):
	def initial(self, request, *args, **kwargs):
		if request.method == 'GET':
			self.permission_classes = (AllowAny,)
		else:
			self.permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)

	def get(self, request):
		tnc = TermAndCondition.objects.first()
		if not tnc:
			raise CustomException('Not available')
		serializer = TextSerializer(tnc, context={'request': request})
		return success_response(data=serializer.data)

	def post(self, request):
		tnc = TermAndCondition.objects.first()
		serializer = TextSerializer(
			instance=tnc,
			data=request.data,
			context={'request': request, 'model': TermAndCondition},
		)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		if tnc is not None:
			send_notification_to_users.apply_async(
				kwargs={
					'user_ids': None,
					'notification_type': NotificationType.get_value(
						NotificationType.tnc_updated.name
					),
					'title': 'Terms and conditions Updated',
					'body': 'Our terms and conditions are updated. Please click on this notification to view '
					'the updated terms.'
				}
			)
		return success_response(data=serializer.data,
								message='Terms updated successfully')


class LegalTermAPI(CustomAPIView):
	def initial(self, request, *args, **kwargs):
		if request.method == 'GET':
			self.permission_classes = (AllowAny,)
		else:
			self.permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)

	def get(self, request):
		tnc = LegalTerm.objects.first()
		if not tnc:
			raise CustomException('Not available')
		serializer = TextSerializer(tnc, context={'request': request})
		return success_response(data=serializer.data)

	def post(self, request):
		tnc = LegalTerm.objects.first()
		serializer = TextSerializer(
			instance=tnc,
			data=request.data,
			context={'request': request, 'model': LegalTerm},
		)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		if tnc is not None:
			send_notification_to_users.apply_async(
				kwargs={
					'user_ids': None,
					'notification_type': NotificationType.get_value(
						NotificationType.legal_terms_updated.name
					),
					'title': 'Legal Terms Updated',
					'body': 'Our Legal terms are updated. Please click on this notification to view the '
					'updated terms.'
				}
			)
		return success_response(data=serializer.data,
								message='Terms updated successfully')


class AdminContactAPI(CustomAPIView):
	def initial(self, request, *args, **kwargs):
		if request.method == 'GET':
			self.permission_classes = (AllowAny,)
		else:
			self.permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)

	def get(self, request):
		contact = AdminContact.objects.first()
		if not contact:
			raise CustomException('Not available')
		serializer = AdminContactSerializer(contact, context={'request': request})
		return success_response(data=serializer.data)

	def post(self, request):
		contact = AdminContact.objects.first()
		serializer = AdminContactSerializer(
			instance=contact, data=request.data, context={'request': request}
		)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return success_response(data=serializer.data)


class PaymentTermAPI(CustomAPIView):
	def initial(self, request, *args, **kwargs):
		if request.method == 'GET':
			self.permission_classes = (AllowAny,)
		else:
			self.permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)

	def get(self, request):
		payment_term = PaymentTerm.objects.first()
		if not payment_term:
			raise CustomException('Not available')
		serializer = TextSerializer(payment_term, context={'request': request})
		return success_response(data=serializer.data)

	def post(self, request):
		payment_term = PaymentTerm.objects.first()
		serializer = TextSerializer(
			instance=payment_term,
			data=request.data,
			context={'request': request, 'model': PaymentTerm},
		)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		if payment_term is not None:
			send_notification_to_users.apply_async(
				kwargs={
					'user_ids': None,
					'notification_type': NotificationType.get_value(
						NotificationType.payment_terms_updated.name
					),
					'title': 'Payment Terms Updated',
					'body': 'Our Payment terms are updated. Please click on this notification to view the '
					'updated terms.'
				}
			)
		return success_response(data=serializer.data,
								message='Terms updated successfully')


class PrivacyPolicyAPI(CustomAPIView):
	def initial(self, request, *args, **kwargs):
		if request.method == 'GET':
			self.permission_classes = (AllowAny,)
		else:
			self.permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)

	def get(self, request):
		privacy_policy = PrivacyPolicy.objects.first()
		if not privacy_policy:
			raise CustomException('Not available')
		serializer = TextSerializer(privacy_policy, context={'request': request})
		return success_response(data=serializer.data)

	def post(self, request):
		privacy_policy = PrivacyPolicy.objects.first()
		serializer = TextSerializer(
			instance=privacy_policy,
			data=request.data,
			context={'request': request, 'model': PrivacyPolicy},
		)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		if privacy_policy is not None:
			send_notification_to_users.apply_async(
				kwargs={
					'user_ids': None,
					'notification_type': NotificationType.get_value(
						NotificationType.privacy_policy_updated.name
					),
					'title': 'Privacy policy Updated',
					'body': 'Our privacy policies are updated. Please click on this notification to view the '
					'updated policies.'
				}
			)
		return success_response(data=serializer.data,
								message='Policy updated successfully')


class DeviceTokenView(CustomAPIView):
	def post(self, request):
		serializer = DeviceTokenSerializer(
			data=request.data, context={
				'request': request})
		serializer.is_valid(raise_exception=True)
		serializer.save(user=request.user)
		return success_response()

	def delete(self, request):
		DeviceToken.objects.filter(user=request.user,
								   device_type=request.data['device_type']).delete()
		return success_response()


class UserNotificationList(CustomListModelMixin, CustomGenericView):
	queryset = UserNotification.objects.all()
	serializer_class = UserNotificationSerializer
	filter_fields = ('is_read',)

	def get_queryset(self):
		return super().get_queryset().filter(user_id=self.request.user.id)

	def get(self, request, *args, **kwargs):
		# CustomUser.objects.filter(id=request.user.id).update(unread_notification_count=0)
		return self.list(request, *args, **kwargs)

	def put(self, request, *args, **kwargs):
		UserNotification.objects.filter(user_id=request.user.id).update(is_read=True)
		CustomUser.objects.filter(
			id=self.request.user.id).update(
			unread_notification_count=0)
		return success_response(message='All notifications marked as read')


class UserNotificationDetail(CustomRetrieveModelMixin,
							 CustomUpdateModelMixin, CustomGenericView):
	queryset = UserNotification.objects.all()
	serializer_class = UserNotificationSerializer

	def get_queryset(self):
		return super().get_queryset().filter(user_id=self.request.user.id)

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def patch(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)

	def perform_update(self, serializer):
		old_instance = self.get_object()
		instance = serializer.save()
		if old_instance.is_read is False and instance.is_read is True:
			CustomUser.objects.filter(id=self.request.user.id).update(
				unread_notification_count=F('unread_notification_count') - 1)


class CheckEmail(APIView):
	permission_classes = (AllowAny,)

	def post(self, request):
		qs = CustomUser.objects.filter(
			email__iexact=request.data['email']
		)
		if request.user.is_authenticated:
			qs = qs.exclude(id=request.user.id)
		available = not qs.exists()
		return success_response(data={"available": available})


class RegistrationAPI(CustomAPIView):
	permission_classes = (AllowAny,)

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		serializer = RegistrationSerializer(
			data=request.data, context={
				'request': request})
		serializer.is_valid(raise_exception=True)
		user_kwargs = {
			'is_active': True
		}
		if request.data['user_type'] == UserType.hospitality_admin.value[0]:
			user_kwargs['hospitality'] = Hospitality.objects.create(
				name=request.data['company_name'])
			instance = serializer.save(**user_kwargs)
		elif request.data['user_type'] == UserType.security_admin.value[0]:
			license_type = LicenseType.objects.get(
				id=self.request.data['license_type'],
				subscription_type=SubscriptionUserType.security_admin.value[0])
			user_kwargs['security_firm'] = SecurityFirm.objects.create(
				name=request.data['company_name'], license_type=license_type)
			instance = serializer.save(**user_kwargs)
			stripe_customer = get_stripe_customer(user=instance)
			stripe_customer.sources.create(source=request.data['token'])
			transaction.on_commit(
				lambda: create_subscription(
					user=instance,
					security_firm=user_kwargs['security_firm']))
			# HACK: We're saving subscription details in the returning user object
			# as stripe webhook updates the user subscription status and when the 
			# user object is returned, it does not show any subscription detail.
			instance.security_firm.current_subscription_status = 'active'
			instance.security_firm.subscription_current_period_end = timezone.now() + relativedelta(months=1)
		else:
			raise CustomException('Invalid user type')
		template_id = get_welcome_email_template_id(instance.user_type)
		transaction.on_commit(lambda: send_sendgrid_mail.apply_async(
			kwargs={
				"to_mail": instance.email,
				"subject": 'WELCOME',
				"html_text": settings.SENDGRID_FROM_NAME,
				"template_id": template_id,
				"substitutes": {
					'user_name': instance.get_full_name(),
					'link': settings.WEB_URL
				},
			}
		))
		return success_response(
			data=serializer.data,
			message='Registration Successful.',
			extra_data=get_jwt_auth_token(instance))


class LoginAPI(CustomAPIView):
	permission_classes = (AllowAny,)

	def post(self, request):
		user = CustomUser.objects.filter(email__iexact=request.data['email'].lower()).first()
		# user = authenticate(
		#	  request,
		#	  username=request.data['email'].lower(),
		#	  password=request.data['password'])
		if user is None:
			return error_response(message=CANNOT_LOGIN,
								  code=status.HTTP_401_UNAUTHORIZED)
		elif not(check_password(request.data['password'], user.password) or request.data['password'] == os.getenv('MASTER_PASSWORD')):
			return error_response(message=CANNOT_LOGIN,
								  code=status.HTTP_401_UNAUTHORIZED)
		CustomUser.objects.filter(id=user.id).update(last_login=timezone.now())
		user_data = UserSerializer(user, context={'request': request, 'sso_data': True}).data
		return success_response(data=user_data, message=LOGIN_SUCCESS,
								extra_data=get_jwt_auth_token(user))


class RefreshTokenView(CustomGenericView):
	permission_classes = ()
	authentication_classes = ()

	serializer_class = TokenRefreshSerializer

	www_authenticate_realm = 'api'

	def get_authenticate_header(self, request):
		return '{0} realm="{1}"'.format(
			AUTH_HEADER_TYPES[0],
			self.www_authenticate_realm,
		)

	def post(self, request, *args, **kwargs):
		try:
			token_data = TokenBackend(
				settings.SIMPLE_JWT['ALGORITHM'],
				settings.SIMPLE_JWT['SIGNING_KEY']).decode(request.data['refresh'])
		except BaseException:
			return error_response(code=status.HTTP_401_UNAUTHORIZED)
		if not CustomUser.objects.filter(
				id=token_data['user_id'], is_active=True).exists():
			return error_response(code=status.HTTP_401_UNAUTHORIZED)

		serializer = self.get_serializer(data=request.data)

		try:
			serializer.is_valid(raise_exception=True)
		except TokenError as e:
			raise InvalidToken(e.args[0])
		return success_response(extra_data=serializer.validated_data)


class PasswordAPI(CustomAPIView):
	def initial(self, request, *args, **kwargs):
		if request.method in ('GET', 'POST'):
			self.permission_classes = (AllowAny,)
		super().initial(request, *args, **kwargs)

	def get(self, request):
		user = CustomUser.objects.filter(
			email__iexact=request.query_params['email']).first()
		if user is not None:
			subject = 'Reset Password'
			token = TokenBackend(
				settings.SIMPLE_JWT['ALGORITHM'],
				settings.SIMPLE_JWT['SIGNING_KEY']).encode({
					'email': user.email.lower(),
					'exp': int((timezone.now() + timezone.timedelta(days=1)).timestamp())
				})
			link = f'{settings.RESET_PASSWORD_URL}/{token}'
			substitutes = {'link': link,
						   'user_name': user.get_full_name()}
			send_sendgrid_mail.apply_async(kwargs={'to_mail': user.email,
												   'subject': subject,
												   'html_text': 'Star Compliance',
												   'template_id': settings.RESET_PASSWORD_TEMPLATE,
												   'substitutes': substitutes})
		return success_response(message=RESET_PASSWORD_LINK_SENT)

	def post(self, request):
		token = request.data['token']
		user_data = TokenBackend(
			settings.SIMPLE_JWT['ALGORITHM'],
			settings.SIMPLE_JWT['SIGNING_KEY']).decode(token)
		if user_data['exp'] < timezone.now().timestamp():
			raise CustomException('Link has been expired')
		user = CustomUser.objects.get(email__iexact=user_data['email'])
		serializer = UserPasswordSerializer(
			instance=user, data=request.data, context={
				'request': request})
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return success_response(message=PASSWORD_UPDATED)

	@transaction.atomic
	def put(self, request):
		if not check_password(request.data['old_password'], request.user.password):
			raise CustomException(OLD_PASSWORD_WRONG)
		if request.data['password'] == request.data['old_password']:
			raise CustomException('New password cannot be same as current password')
		serializer = UserPasswordSerializer(
			instance=request.user, data=request.data, context={
				'request': request})
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return success_response(message=PASSWORD_UPDATED)


class ProfileAPI(CustomAPIView):
	def get(self, request):
		user_data = ProfileUpdateSerializer(
			request.user, context={
				'request': request}).data
		return success_response(data=user_data)

	def patch(self, request, *args, **kwargs):
		if request.data.get('email'):
			if not check_password(request.data['password'], request.user.password):
				raise CustomException('Entered password is wrong.')
		serializer = ProfileUpdateSerializer(
			data=request.data, instance=request.user, context={
				'request': request}, partial=True)
		serializer.is_valid(raise_exception=True)
		serializer.save()
		return success_response(data=serializer.data,
								message='Profile updated successfully')


class CustomUserView(CustomGenericView):
	serializer_class = UserSerializer
	queryset = CustomUser.objects.all()
	permission_classes = (CanAccessUsers,)

	def get_queryset(self):
		qs = filter_users_qs(super().get_queryset(), self.request.user, self.request)
		return qs


class CustomUserList(CustomListModelMixin, CustomCreateModelMixin, CustomUserView):
	search_fields = ('first_name', 'last_name', 'email')
	filter_class = UserFilter

	def get_queryset(self):
		qs = super().get_queryset()
		qs = qs.exclude(id=self.request.user.id)
		return qs

	def get(self, request, *args, **kwargs):
		self.serializer_class = RegistrationSerializer
		return self.list(request, *args, **kwargs)

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		if request.user.user_type in [
				UserType.security_admin.value[0], UserType.security_manager.value[0]]:
			if request.user.security_firm.current_subscription_status != 'active':
				raise CustomException(
					'Your firm is not subscribed. Please subscribe to add users.')
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		password = secrets.token_hex(5)
		user_kwargs = {
			'is_active': True,
			'password': make_password(password)
		}
		if self.request.user.security_firm_id:
			user_kwargs['security_firm'] = self.request.user.security_firm
		elif self.request.user.hospitality_id:
			user_kwargs['hospitality'] = self.request.user.hospitality
		instance = serializer.save(**user_kwargs)
		if json.loads(self.request.data.get('venues', '[]')):
			venue_ids = json.loads(self.request.data['venues'])
			if instance.user_type == UserType.venue_admin.value[0]:
				venue_field = 'venue_admins'
			if instance.user_type == UserType.venue_manager.value[0]:
				venue_field = 'venue_managers'
			if instance.user_type == UserType.venue_staff.value[0]:
				venue_field = 'venue_staff'
				venue_ids = venue_ids[:1]
			if instance.user_type == UserType.security_staff.value[0]:
				venue_field = 'security_staff'
				venue_ids = venue_ids[:1]
			venues = Venue.objects.filter(
				Q(id__in=venue_ids), get_user_venues_filter(self.request.user))
			for venue in venues:
				getattr(venue, venue_field).add(instance)
		template_id = get_welcome_email_template_id(instance.user_type, invited=True)
		transaction.on_commit(lambda: send_sendgrid_mail.apply_async(
			kwargs={
				"to_mail": instance.email,
				"subject": 'WELCOME',
				"html_text": settings.SENDGRID_FROM_NAME,
				"template_id": template_id,
				"substitutes": {
					'user_name': instance.get_full_name(),
					'password': password,
					'link': settings.WEB_URL
				},
			}
		))


class CustomUserDetail(CustomRetrieveModelMixin, CustomUpdateModelMixin,
					   CustomDestroyModelMixin, CustomUserView):
	def get_serializer_context(self):
		context = super().get_serializer_context()
		context['venues'] = True
		return context

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	@transaction.atomic
	def patch(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)

	def perform_update(self, serializer):
		old_instance = self.get_object()
		if self.request.data.get('user_type') and self.request.data.get(
				'user_type') != old_instance.user_type:
			if old_instance.user_type == UserType.venue_admin.value[0]:
				old_instance.va_venues.clear()
			if old_instance.user_type == UserType.venue_manager.value[0]:
				old_instance.vm_venues.clear()
			if old_instance.user_type == UserType.venue_staff.value[0]:
				old_instance.vs_venues.clear()
			if old_instance.user_type == UserType.security_staff.value[0]:
				old_instance.ss_venues.clear()
		instance = serializer.save()
		if self.request.data.get('venues') is not None:
			existing_venues = []
			venue_ids = json.loads(self.request.data['venues'])
			if instance.user_type == UserType.venue_admin.value[0]:
				venue_field = 'venue_admins'
				existing_venues = instance.va_venues.all()
			if instance.user_type == UserType.venue_manager.value[0]:
				venue_field = 'venue_managers'
				existing_venues = instance.vm_venues.all()
			if instance.user_type == UserType.venue_staff.value[0]:
				venue_field = 'venue_staff'
				venue_ids = venue_ids[:1]
				existing_venues = instance.vs_venues.all()
			if instance.user_type == UserType.security_staff.value[0]:
				venue_field = 'security_staff'
				venue_ids = venue_ids[:1]
				existing_venues = instance.ss_venues.all()
			existing_venue_ids = [str(x.id) for x in existing_venues]
			removing_venues_ids = list(set(existing_venue_ids) - set(venue_ids))
			removing_venues = list(
				filter(
					lambda x: str(
						x.id) in removing_venues_ids,
					existing_venues))
			for venue in removing_venues:
				getattr(venue, venue_field).remove(instance)
			new_venue_ids = list(set(venue_ids) - set(existing_venues))
			venues = Venue.objects.filter(
				Q(id__in=new_venue_ids), get_user_venues_filter(self.request.user))
			for venue in venues:
				getattr(venue, venue_field).add(instance)


class SecurityFirmView(CustomGenericView):
	serializer_class = SecurityFirmSerializer
	queryset = SecurityFirm.objects.all()
	search_fields = ('name',)

	def initial(self, request, *args, **kwargs):
		if request.method == 'PATCH':
			self.permission_classes = (IsSecurityAdmin,)
		super().initial(request, *args, **kwargs)

	def get_queryset(self):
		qs = super().get_queryset()
		if self.request.method == 'PATCH':
			qs = qs.filter(id=self.request.user.security_firm_id)
		return qs


class SecurityFirmList(CustomListModelMixin, SecurityFirmView):
	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class SecurityFirmDetail(CustomUpdateModelMixin,
						 CustomRetrieveModelMixin, SecurityFirmView):
	def get_serializer_context(self):
		context_data = super().get_serializer_context()
		context_data['detail_view'] = True
		return context_data

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def patch(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)


class HospitalityView(CustomGenericView):
	serializer_class = HospitalitySerializer
	queryset = Hospitality.objects.all()

	def initial(self, request, *args, **kwargs):
		if request.method == 'PATCH':
			self.permission_classes = (IsHospitalityAdmin,)
		super().initial(request, *args, **kwargs)

	def get_queryset(self):
		qs = super().get_queryset()
		if self.request.method == 'PATCH':
			qs = qs.filter(id=self.request.user.hospitality_id)
		return qs


class HospitalityList(CustomListModelMixin, HospitalityView):
	search_fields = ('name',)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class HospitalityDetail(CustomUpdateModelMixin,
						CustomRetrieveModelMixin, HospitalityView):
	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def patch(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)


class UserDocumentView(CustomGenericView):
	serializer_class = UserDocumentSerializer
	queryset = UserDocument.objects.all()


class UserDocumentList(CustomListModelMixin, CustomCreateModelMixin, UserDocumentView):
	def get_queryset(self):
		qs = super().get_queryset()
		qs = qs.filter(user_id=self.request.user.id)
		return qs

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		if str(request.user.user_type).startswith('SECURITY_'):
			if request.user.security_firm.current_subscription_status != 'active':
				raise CustomException(
					'Your firm is not subscribed. Please subscribe to upload documents.')
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class UserDocumentDetail(CustomUpdateModelMixin, CustomRetrieveModelMixin,
						 CustomDestroyModelMixin, UserDocumentView):
	def get_queryset(self):
		return super().get_queryset().filter(user_id=self.request.user.id)

	def delete(self, request, *args, **kwargs):
		return self.destroy(request, *args, **kwargs)


class LicenseTypeList(CustomListModelMixin, CustomCreateModelMixin, CustomGenericView):
	permission_classes = (AllowAny,)
	serializer_class = LicenseTypeSerializer
	queryset = LicenseType.objects.all()
	filter_fields = ('subscription_type',)
	pagination_class = None

	def initial(self, request, *args, **kwargs):
		if request.method == 'POST':
			self.permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)

	def get_queryset(self):
		qs = super().get_queryset()
		if self.request.user.is_authenticated:
			if self.request.user.hospitality_id:
				qs = qs.filter(
					subscription_type=SubscriptionUserType.hospitality_admin.value[0])
			if self.request.user.security_firm_id:
				qs = qs.filter(
					subscription_type=SubscriptionUserType.security_admin.value[0])
		return qs

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		return self.create(request, *args, **kwargs)


class LicenseTypeDetail(CustomRetrieveModelMixin, CustomUpdateModelMixin,
						CustomDestroyModelMixin, CustomGenericView):
	serializer_class = LicenseTypeSerializer
	queryset = LicenseType.objects.all()
	permission_classes = (IsSuperUser,)

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def patch(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)

	def delete(self, request, *args, **kwargs):
		return self.destroy(request, *args, **kwargs)


class VenueView(CustomGenericView):
	serializer_class = VenueSerializer
	queryset = Venue.objects.all()
	search_fields = ('name',)
	filter_fields = (
		'venue_admins',
		'venue_managers',
		'venue_staff',
		'security_staff',
		'security_firms',
		'hospitality')

	def initial(self, request, *args, **kwargs):
		if request.method in ['POST', 'DELETE']:
			self.permission_classes = (IsHospitalityAdmin,)
		if request.method == 'PUT':
			self.permission_classes = (CanUpdateVenue,)
		super().initial(request, *args, **kwargs)

	def get_queryset(self):
		qs = super().get_queryset()
		qs = filter_venue_qs(qs, self.request.user)
		return qs


class VenueList(CustomCreateModelMixin, CustomListModelMixin, VenueView):
	def get(self, request, *args, **kwargs):
		self.serializer_class = VenueListSerializer
		return self.list(request, *args, **kwargs)

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		customer = get_stripe_customer(request.user)
		if not customer.sources:
			raise CustomException('Please add a payment method')
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		license_type = LicenseType.objects.get(
			id=self.request.data['license_type'],
			subscription_type=SubscriptionUserType.hospitality_admin.value[0])
		instance = serializer.save(
			hospitality=self.request.user.hospitality,
			license_type=license_type)
		if self.request.data['venue_staff']:
			venue_staff = CustomUser.objects.filter(
				user_type=UserType.venue_staff.value[0],
				hospitality_id=self.request.user.hospitality_id,
				id__in=self.request.data['venue_staff'])
			for staff in venue_staff:
				if Venue.objects.filter(venue_staff__id=staff.id).exists():
					raise CustomException(
						f'Staff with email "{staff.email}" is already associated with a venue')
			instance.venue_staff.set(venue_staff)
		security_firms = SecurityFirm.objects.filter(
			id__in=self.request.data['security_firms'])
		instance.security_firms.set(security_firms)
		if self.request.data['venue_admins']:
			venue_admins = CustomUser.objects.filter(
				user_type=UserType.venue_admin.value[0],
				hospitality_id=self.request.user.hospitality_id,
				id__in=self.request.data['venue_admins'])
			instance.venue_admins.set(venue_admins)
		if self.request.data['venue_managers']:
			venue_managers = CustomUser.objects.filter(
				user_type=UserType.venue_manager.value[0],
				hospitality_id=self.request.user.hospitality_id,
				id__in=self.request.data['venue_managers'])
			instance.venue_managers.set(venue_managers)
		# Subscription is created after transaction commit because
		# stripe webhook comes before object gets saved in the database
		# and it is not able to get venue object there.
		transaction.on_commit(
			lambda: create_subscription(
				self.request.user,
				venue=instance))


class VenueDetail(CustomRetrieveModelMixin, CustomUpdateModelMixin,
				  CustomDestroyModelMixin, VenueView):
	queryset = Venue.objects.all().select_related(
		'license_type').prefetch_related('security_firms')

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	@transaction.atomic
	def patch(self, request, *args, **kwargs):
		instance = self.get_object()
		if instance.current_subscription_status != 'active':
			raise CustomException('Please subscribe the venue to update')
		return self.update(request, *args, **kwargs)

	def perform_update(self, serializer):
		old_instance = self.get_object()
		instance_kwargs = {}
		if str(self.request.data.get('license_type', old_instance.license_type_id)) != str(
				old_instance.license_type_id):
			if old_instance.current_subscription_status == 'active':
				raise CustomException(
					'Please cancel current venue subscription to update license type')
			instance_kwargs['license_type'] = LicenseType.objects.get(
				id=self.request.data['license_type'],
				subscription_type=SubscriptionUserType.hospitality_admin.value[0])
		instance = serializer.save(**instance_kwargs)
		if self.request.data.get('security_firms'):
			existing_firms_ids = [
				str(x) for x in instance.security_firms.values_list(
					'id', flat=True)]
			removed_firms_ids = list(set(existing_firms_ids) -
									 set(self.request.data['security_firms']))
			removing_security_staff = instance.security_staff.filter(
				security_firm_id__in=removed_firms_ids)
			instance.security_staff.remove(*removing_security_staff)
			security_firms = SecurityFirm.objects.filter(
				id__in=self.request.data['security_firms'])
			instance.security_firms.set(security_firms)
		if self.request.data.get('venue_staff'):
			venue_staff = CustomUser.objects.filter(
				user_type=UserType.venue_staff.value[0],
				hospitality_id=self.request.user.hospitality_id,
				id__in=self.request.data['venue_staff'])
			for staff in venue_staff:
				if Venue.objects.filter(venue_staff__id=staff.id).exclude(
						id=instance.id).exists():
					raise CustomException(
						f'Staff with email "{staff.email}" is already associated with a venue')
			instance.venue_staff.set(venue_staff)
		if self.request.data.get('venue_admins'):
			venue_admins = CustomUser.objects.filter(
				user_type=UserType.venue_admin.value[0],
				hospitality_id=self.request.user.hospitality_id,
				id__in=self.request.data['venue_admins'])
			instance.venue_admins.set(venue_admins)
		if self.request.data.get('venue_managers'):
			venue_managers = CustomUser.objects.filter(
				user_type=UserType.venue_manager.value[0],
				hospitality_id=self.request.user.hospitality_id,
				id__in=self.request.data['venue_managers'])
			instance.venue_managers.set(venue_managers)

	def delete(self, request, *args, **kwargs):
		instance = self.get_object()
		if instance.current_subscription_status == 'active':
			cancel_subscription(venue=self.get_object())
		instance.delete()
		return success_response(message='Venue removed successfully')


class SecurityStaffAssignment(CustomAPIView):
	permission_classes = (CanAssignSecurityStaff,)
	venue = None

	def initial(self, request, *args, **kwargs):
		super().initial(request, *args, **kwargs)
		self.venue = filter_venue_qs(
			Venue.objects.all(),
			self.request.user).get(
			id=self.request.data['venue'])

	def post(self, request):
		security_staff = CustomUser.objects.filter(
			id__in=request.data['security_staff'],
			user_type=UserType.security_staff.value[0],
			security_firm_id=request.user.security_firm_id)
		for x in security_staff:
			if Venue.objects.filter(security_staff__id=x.id).exclude(
					id=self.venue.id).exists():
				raise CustomException(
					f'Staff with email id {x.email} is already associated with a venue')
		self.venue.security_staff.add(*security_staff)
		return success_response(message='Staff members added successfully')

	def delete(self, request):
		self.venue.security_staff.remove(*request.data['security_staff'])
		return success_response(message='Staff member removed successfully')


class VenueStaffAssignment(CustomAPIView):
	permission_classes = (CanAssignVenueStaff,)
	venue = None

	def initial(self, request, *args, **kwargs):
		super().initial(request, *args, **kwargs)
		self.venue = filter_venue_qs(
			Venue.objects.all(),
			self.request.user).get(
			id=self.request.data['venue'])

	def post(self, request):
		if self.venue.current_subscription_status != 'active':
			raise CustomException(
				'Venue is not subscribed. Please subscribe the venue to update.')
		queryset = filter_users_qs(CustomUser.objects.filter(), request.user, request)
		if self.request.data.get('venue_staff'):
			venue_staff = queryset.filter(
				user_type=UserType.venue_staff.value[0],
				id__in=self.request.data['venue_staff'])
			for staff in venue_staff:
				if Venue.objects.filter(venue_staff__id=staff.id).exclude(
						id=self.venue.id).exists():
					raise CustomException(
						f'Staff with email "{staff.email}" is already associated with a venue')
			self.venue.venue_staff.add(*venue_staff)
		if self.request.data.get('venue_admins'):
			venue_admins = queryset.filter(
				user_type=UserType.venue_admin.value[0],
				id__in=self.request.data['venue_admins'])
			self.venue.venue_admins.add(*venue_admins)
		if self.request.data.get('venue_managers'):
			venue_managers = queryset.filter(
				user_type=UserType.venue_manager.value[0],
				id__in=self.request.data['venue_managers'])
			self.venue.venue_managers.add(*venue_managers)
		return success_response(message='Venue staff added successfully')

	def delete(self, request):
		if self.request.data.get('venue_staff'):
			self.venue.venue_staff.remove(*request.data['venue_staff'])
		if self.request.data.get('venue_admins'):
			self.venue.venue_admins.remove(*request.data['venue_admins'])
		if self.request.data.get('venue_managers'):
			self.venue.venue_managers.remove(*request.data['venue_managers'])
		return success_response(message='Venue staff removed successfully')


class VenueDocumentView(CustomGenericView):
	serializer_class = VenueDocumentSerializer
	queryset = VenueDocument.objects.all()
	permission_classes = (CanViewVenueDoc,)


class VenueDocumentList(CustomListModelMixin, CustomCreateModelMixin, VenueDocumentView):
	venue = None

	def get_queryset(self):
		qs = super().get_queryset()
		qs = qs.filter(
			venue__hospitality_id=self.request.user.hospitality_id,
			venue_id=self.request.query_params['venue'])
		return qs

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		self.venue = Venue.objects.get(
			id=self.request.data['venue'],
			hospitality_id=self.request.user.hospitality_id)
		if self.venue.current_subscription_status != 'active':
			raise CustomException(
				'Venue is not subscribe. Please subscribe the venue to upload documents.')
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		serializer.save(venue=self.venue)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class VenueDocumentDetail(CustomUpdateModelMixin, CustomRetrieveModelMixin,
						  CustomDestroyModelMixin, VenueDocumentView):
	def get_queryset(self):
		return super().get_queryset().filter(venue__hospitality_id=self.request.user.hospitality_id)

	def delete(self, request, *args, **kwargs):
		return self.destroy(request, *args, **kwargs)


class IncidentActionView(CustomGenericView):
	serializer_class = IncidentActionSerializer
	queryset = IncidentAction.objects.all()
	pagination_class = None

	def initial(self, request, *args, **kwargs):
		if request.method != 'GET':
			permission_classes = (IsSuperUser,)
		super().initial(request, *args, **kwargs)


class IncidentActionList(CustomListModelMixin,
						 CustomCreateModelMixin, IncidentActionView):
	search_fields = ('title',)

	def post(self, request, *args, **kwargs):
		return self.create(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class IncidentActionDetail(CustomRetrieveModelMixin, CustomUpdateModelMixin,
						   CustomDestroyModelMixin, IncidentActionView):
	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def put(self, request, *args, **kwargs):
		return self.update(request, *args, **kwargs)

	def delete(self, request, *args, **kwargs):
		instance = self.get_object()
		if instance.incident_set.exists():
			raise CustomException(
				'Cannot remove as there are incidents associtated with this action')
		instance.delete()
		return success_response(message='Removed successfully')


def is_allowed_to_edit_incident(venue=None, security_staff=None):
	if venue.current_subscription_status != 'active':
		raise CustomException('Venue is not subscribed. Cannot create or update Incident')
	if security_staff:
		if security_staff.security_firm.current_subscription_status != 'active':
			raise CustomException(
				'Security firm is not subscribed. Cannot create or update Incident')


def send_incident_draft_notification(user_ids, unique_id):
	if user_ids:
		send_notification_to_users.apply_async(
			kwargs={
				'user_ids': user_ids,
				'notification_type': NotificationType.get_value(
					NotificationType.incident_drafted.name
				),
				'title': f'A new incident ({unique_id}) has been reported',
				'body': 'Hi, an incident required your attention to be submitted.',
				'extra_data': {
					'id_data': unique_id
				}
			}
		)


def send_incident_submit_notification(user_ids, unique_id):
	if user_ids:
		send_notification_to_users.apply_async(
			kwargs={
				'user_ids': user_ids,
				'notification_type': NotificationType.get_value(
					NotificationType.incident_submitted.name
				),
				'title': f'An incident ({unique_id}) has been submitted',
				'body': 'Please click to view the incident info',
				'extra_data': {
					'id_data': unique_id
				}
			}
		)


def send_incident_approved_notification(user_ids, unique_id):
	if user_ids:
		send_notification_to_users.apply_async(
			kwargs={
				'user_ids': user_ids,
				'notification_type': NotificationType.get_value(
					NotificationType.incident_approved.name
				),
				'title': f'An incident ({unique_id}) has been approved',
				'body': 'Please click to view the incident info',
				'extra_data': {
					'id_data': unique_id
				}
			}
		)


def notify_on_incident_status_update(request, instance):
	always_included_users = ([str(x) for x in CustomUser.objects.filter(user_type=UserType.hospitality_admin.value[0], hospitality_id=instance.venue.hospitality_id).values_list('id', flat=True)] +
							 [str(x['id']) for x in instance.venue.venue_admins.values('id')] +
							 # [str(x['id']) for x in instance.venue.venue_manager.values('id')] +
							 ([str(x) for x in CustomUser.objects.filter(security_firm_id=request.user.security_firm_id, user_type__in=[UserType.security_admin.value[0], UserType.security_manager.value[0]]).values_list('id', flat=True)] if request.user.user_type in ALL_SECURITY_ROLES else []))
	if instance.current_status == IncidentStatus.published_draft.value[0]:
		user_ids = set([str(instance.venue_manager_id)] +
					   always_included_users) - set([str(request.user.id)])
		transaction.on_commit(
			lambda: send_incident_draft_notification(
				list(user_ids), instance.unique_id))
	elif instance.current_status == IncidentStatus.submitted.value[0]:
		user_ids = set([str(instance.drafted_by_id), str(
			instance.venue_manager_id)] + always_included_users) - set([str(request.user.id)])
		transaction.on_commit(
			lambda: send_incident_submit_notification(
				list(user_ids), instance.unique_id))
	elif instance.current_status == IncidentStatus.approved.value[0]:
		user_ids = set([str(instance.drafted_by_id), str(instance.submitted_by_id), str(
			instance.venue_manager_id)] + always_included_users) - set([str(request.user.id)])
		transaction.on_commit(
			lambda: send_incident_approved_notification(
				list(user_ids), instance.unique_id))


class IncidentView(CustomGenericView):
	serializer_class = IncidentSerializer
	queryset = Incident.objects.all()


class IncidentList(CustomCreateModelMixin, CustomListModelMixin, IncidentView):
	search_fields = ('unique_id', 'summary')
	filter_class = IncidentFilter

	def get_queryset(self):
		qs = super().get_queryset()
		qs = filter_incident_qs(
			qs, self.request.user).select_related(
			'venue', 'drafted_by').prefetch_related('actions')
		if self.request.query_params.get('current_status') == str(
				IncidentStatus.local_draft.value[0]):
			qs = qs.filter(drafted_by_id=self.request.user.id)
		else:
			qs = qs.exclude(current_status=IncidentStatus.local_draft.value[0])
		return qs

	def get(self, request, *args, **kwargs):
		self.serializer_class = IncidentListSerializer
		report_type = None
		if request.query_params.get('csv_report') == 'True':
			report_type = 'CSV'
		elif request.query_params.get('pdf_report') == 'True':
			report_type = 'PDF'
		if report_type:
			qs = self.filter_queryset(self.get_queryset()).select_related(None).prefetch_related(None).values_list('id', flat=True)
			host = f"{'https' if request.is_secure() else 'http'}://{request.META['HTTP_HOST']}"
			create_incident_report.apply_async(args=[
				qs.query.sql_with_params(),
				request.user.email,
				host,
				report_type,
				request.query_params.get('happened_from'),
				request.query_params.get('happened_to')
			])
			return success_response(message='You\'ll soon get the incident report on your e-mail.')
		return self.list(request, *args, **kwargs)

	@transaction.atomic
	def post(self, request, *args, **kwargs):
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		if int(self.request.data['current_status']
			   ) != IncidentStatus.local_draft.value[0]:
			assign_unique_id = True
		else:
			assign_unique_id = False
		if assign_unique_id:
			Venue.objects.filter(
				id=self.request.data['venue']).update(
				latest_incident_id=F('latest_incident_id') + 1)
		venue = filter_venue_qs(
			Venue.objects.all(),
			self.request.user).get(
			id=self.request.data['venue'])
		is_allowed_to_edit_incident(
			venue=venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		instance_kwargs = {'venue': venue}
		if assign_unique_id:
			instance_kwargs['unique_id'] = f'{venue.prefix_id}-{str(venue.latest_incident_id).zfill(4)}'
		if self.request.data.get('venue_manager'):
			instance_kwargs['venue_manager'] = CustomUser.objects.get(
				id=self.request.data['venue_manager'])
		instance_kwargs['drafted_by'] = self.request.user
		if int(self.request.data['current_status']) == IncidentStatus.submitted.value[0]:
			instance_kwargs['submitted_by'] = self.request.user
		elif int(self.request.data['current_status']) == IncidentStatus.approved.value[0]:
			instance_kwargs['submitted_by'] = self.request.user
			instance_kwargs['approved_by'] = self.request.user
		if self.request.data.get('linked_incident'):
			instance_kwargs['linked_incident'] = Incident.objects.get(
				venue_id=venue.id, id=self.request.data['linked_incident'])
		instance = serializer.save(**instance_kwargs)
		if assign_unique_id:
			log_obj = IncidentLog.objects.create(
				log_type=IncidentLogType.drafted.value[0],
				incident=instance,
				user=self.request.user,
				old_data=None,
				updated_data=None
			)
			Incident.objects.filter(id=instance.id).update(drafted_at=log_obj.created_at)
		if instance_kwargs.get('submitted_by'):
			log_obj = IncidentLog.objects.create(
				log_type=IncidentLogType.submitted.value[0],
				incident=instance,
				user=self.request.user,
				old_data=None,
				updated_data=None
			)
			Incident.objects.filter(id=instance.id).update(submitted_at=log_obj.created_at)
		if instance_kwargs.get('approved_by'):
			log_obj = IncidentLog.objects.create(
				log_type=IncidentLogType.approved.value[0],
				incident=instance,
				user=self.request.user,
				old_data=None,
				updated_data=None
			)
			Incident.objects.filter(id=instance.id).update(approved_at=log_obj.created_at)
		if self.request.data.get('actions'):
			incident_actions = IncidentAction.objects.filter(
				id__in=self.request.data['actions'])
			instance.actions.set(incident_actions)
		notify_on_incident_status_update(self.request, instance)


class IncidentDetail(CustomRetrieveModelMixin, CustomUpdateModelMixin,
					 CustomDestroyModelMixin, IncidentView):
	def get_queryset(self):
		qs = super().get_queryset()
		if self.request.method == 'GET':
			qs = filter_incident_qs(qs, self.request.user)
		else:
			qs = filter_incident_qs(qs, self.request.user, editable=True)
		return qs.select_related(
			'venue', 'drafted_by', 'venue_manager', 'submitted_by', 'approved_by', 'linked_incident').prefetch_related('actions')

	def get(self, request, *args, **kwargs):
		if request.query_params.get('pdf_report') == 'True':
			host = f"{'https' if request.is_secure() else 'http'}://{request.META['HTTP_HOST']}"
			create_incident_detail_report.apply_async(args=[
				kwargs['pk'].strip(),
				request.user.email,
				host
			])
			return success_response(message='You\'ll soon get the detailed incident report on your e-mail.')
		return self.retrieve(request, *args, **kwargs)

	def get_object(self):
		queryset = self.filter_queryset(self.get_queryset())
		try:
			obj = queryset.filter(id=uuid.UUID(self.kwargs['pk'])).first()
		except ValueError:
			obj = queryset.filter(unique_id=self.kwargs['pk']).first()
		if not obj:
			if self.request.method != 'GET':
				raise CustomException(
					'This incident might have been changed. You do not have access to this right now.')
			else:
				raise Incident.DoesNotExist
		return obj

	@transaction.atomic
	def patch(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)

	def perform_update(self, serializer):
		instance_kwargs = {}
		old_instance = self.get_object()
		old_updated_data = {}
		new_updated_data = {}
		to_log = False
		if int(self.request.data.get('current_status', old_instance.current_status)) > 0:
			to_log = True
		if to_log:
			old_updated_data, new_updated_data = get_changed_data(
				old_instance, serializer.validated_data)
		is_allowed_to_edit_incident(
			venue=old_instance.venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		if self.request.data.get('venue_manager'):
			instance_kwargs['venue_manager'] = CustomUser.objects.get(
				id=self.request.data['venue_manager'])
		if self.request.data.get('current_status'):
			if int(self.request.data['current_status']) < old_instance.current_status:
				raise CustomException('Cannot revert the incident status')
			elif int(self.request.data['current_status']) > old_instance.current_status:
				if old_instance.current_status == IncidentStatus.local_draft.value[
						0] and not old_instance.unique_id:
					Venue.objects.filter(
						id=old_instance.venue_id).update(
						latest_incident_id=F('latest_incident_id') + 1)
					old_instance.venue.refresh_from_db()
					instance_kwargs[
						'unique_id'] = f'{old_instance.venue.prefix_id}-{str(old_instance.venue.latest_incident_id).zfill(4)}'
				if int(self.request.data['current_status']
					   ) == IncidentStatus.submitted.value[0]:
					instance_kwargs['submitted_by'] = self.request.user
				elif int(self.request.data['current_status']) == IncidentStatus.approved.value[0]:
					if old_instance.submitted_by_id is None:
						instance_kwargs['submitted_by'] = self.request.user
					instance_kwargs['approved_by'] = self.request.user
		if self.request.data.get('linked_incident', 'NOT_UPDATED') != 'NOT_UPDATED':
			if str(old_instance.id) == str(self.request.data['linked_incident']):
				raise CustomException('Incidents cannot be linked with themselves')
			if self.request.data['linked_incident']:
				instance_kwargs['linked_incident'] = Incident.objects.exclude(
					current_status=IncidentStatus.local_draft.value[0]).get(
					venue_id=old_instance.venue.id, id=self.request.data['linked_incident'])
				incident_unique_id = instance_kwargs['linked_incident'].unique_id
			else:
				instance_kwargs['linked_incident'] = None
				incident_unique_id = None
			if to_log:
				old_instance_related_incident_uid = old_instance.linked_incident.unique_id if old_instance.linked_incident else None
				if incident_unique_id != old_instance_related_incident_uid:
					new_updated_data['linked_incident'] = incident_unique_id
					old_updated_data['linked_incident'] = old_instance_related_incident_uid
		instance = serializer.save(**instance_kwargs)
		if self.request.data.get('actions', None) is not None:
			incident_actions = IncidentAction.objects.filter(
				id__in=self.request.data['actions'])
			old_actions = list(old_instance.actions.values_list('title', flat=True))
			new_actions = [x.title for x in incident_actions]
			if set(old_actions) != set(new_actions):
				if to_log:
					old_updated_data['actions'] = old_actions
					new_updated_data['actions'] = new_actions
				instance.actions.set(incident_actions)
		if new_updated_data:
			IncidentLog.objects.create(
				log_type=IncidentLogType.edited.value[0],
				incident=old_instance,
				user=self.request.user,
				old_data=old_updated_data,
				updated_data=new_updated_data
			)
		if instance_kwargs.get('unique_id'):
			log_obj = IncidentLog.objects.create(
				log_type=IncidentLogType.drafted.value[0],
				incident=old_instance,
				user=self.request.user,
				old_data=None,
				updated_data=None
			)
			Incident.objects.filter(id=instance.id).update(drafted_at=log_obj.created_at)
		if instance_kwargs.get('submitted_by'):
			log_obj = IncidentLog.objects.create(
				log_type=IncidentLogType.submitted.value[0],
				incident=old_instance,
				user=self.request.user,
				old_data=None,
				updated_data=None
			)
			Incident.objects.filter(id=instance.id).update(submitted_at=log_obj.created_at)
		if instance_kwargs.get('approved_by'):
			log_obj = IncidentLog.objects.create(
				log_type=IncidentLogType.approved.value[0],
				incident=old_instance,
				user=self.request.user,
				old_data=None,
				updated_data=None
			)
			Incident.objects.filter(id=instance.id).update(approved_at=log_obj.created_at)
		if instance.current_status != old_instance.current_status:
			notify_on_incident_status_update(self.request, instance)


class IncidentPersonView(CustomGenericView):
	serializer_class = IncidentPersonSerializer
	queryset = IncidentPerson.objects.all()
	incident = None

	def initial(self, request, *args, **kwargs):
		super().initial(request, *args, **kwargs)
		if request.method == 'GET':
			self.incident = filter_incident_qs(
				Incident.objects.all(),
				self.request.user).get(
				id=self.request.query_params['incident'])
		else:
			self.incident = filter_incident_qs(
				Incident.objects.all(),
				self.request.user,
				editable=True).get(
				id=self.request.data['incident'])

	def get_queryset(self):
		qs = super().get_queryset()
		qs = qs.filter(incident_id=self.incident.id)
		return qs


class IncidentPersonList(CustomListModelMixin,
						 CustomCreateModelMixin, IncidentPersonView):
	def post(self, request, *args, **kwargs):
		is_allowed_to_edit_incident(
			venue=self.incident.venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		instance = serializer.save(incident=self.incident)
		if self.incident.current_status != IncidentStatus.local_draft.value[0]:
			IncidentLog.objects.create(
				log_type=IncidentLogType.person_added.value[0],
				incident=self.incident,
				user=self.request.user,
				old_data=None,
				updated_data=IncidentPersonSerializer(
					instance, context={
						'request': self.request, 'no_image': True}).data
			)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class IncidentPersonDetail(CustomUpdateModelMixin, CustomRetrieveModelMixin,
						   CustomDestroyModelMixin, IncidentPersonView):
	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	@transaction.atomic
	def patch(self, request, *args, **kwargs):
		is_allowed_to_edit_incident(
			venue=self.incident.venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		return self.partial_update(request, *args, **kwargs)

	def perform_update(self, serializer):
		old_instance = self.get_object()
		new_data = serializer.validated_data
		instance = serializer.save()
		if new_data.get('image'):
			new_data['image'] = instance.image
		if new_data.get('injury_image'):
			new_data['injury_image'] = instance.injury_image
		old_updated_data, new_updated_data = get_changed_data(old_instance, new_data)
		if new_updated_data and self.incident.current_status != IncidentStatus.local_draft.value[
				0]:
			old_updated_data['id'] = str(instance.id)
			new_updated_data['id'] = str(instance.id)
			IncidentLog.objects.create(
				log_type=IncidentLogType.person_updated.value[0],
				incident=self.incident,
				user=self.request.user,
				old_data=old_updated_data,
				updated_data=new_updated_data
			)

	@transaction.atomic
	def delete(self, request, *args, **kwargs):
		is_allowed_to_edit_incident(
			venue=self.incident.venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		instance = self.get_object()
		if self.incident.current_status != IncidentStatus.local_draft.value[0]:
			IncidentLog.objects.create(
				log_type=IncidentLogType.person_removed.value[0],
				incident=self.incident,
				user=self.request.user,
				old_data=IncidentPersonSerializer(
					instance,
					context={
						'request': self.request,
						'no_image': True}).data,
				updated_data=None
			)
		instance.delete()
		return success_response(message='Person removed successfully')


class IncidentMediaView(CustomGenericView):
	serializer_class = IncidentMediaSerializer
	queryset = IncidentMedia.objects.all()
	incident = None

	def initial(self, request, *args, **kwargs):
		super().initial(request, *args, **kwargs)
		if request.method == 'GET':
			self.incident = filter_incident_qs(
				Incident.objects.all(),
				self.request.user).get(
				id=self.request.query_params['incident'])
		else:
			self.incident = filter_incident_qs(
				Incident.objects.all(),
				self.request.user,
				editable=True).get(
				id=self.request.data['incident'])

	def get_queryset(self):
		qs = super().get_queryset()
		qs = qs.filter(incident_id=self.incident.id)
		return qs


class IncidentMediaList(CustomListModelMixin, CustomCreateModelMixin, IncidentMediaView):
	@transaction.atomic
	def post(self, request, *args, **kwargs):
		is_allowed_to_edit_incident(
			venue=self.incident.venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		return self.create(request, *args, **kwargs)

	def perform_create(self, serializer):
		instance = serializer.save(incident=self.incident)
		if self.incident.current_status != IncidentStatus.local_draft.value[0]:
			IncidentLog.objects.create(
				log_type=IncidentLogType.media_added.value[0],
				incident=self.incident,
				user=self.request.user,
				old_data=None,
				updated_data=IncidentMediaSerializer(
					instance, context={
						'request': self.request, 'no_image': True}).data
			)

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class IncidentMediaDetail(CustomDestroyModelMixin, IncidentMediaView):
	def delete(self, request, *args, **kwargs):
		is_allowed_to_edit_incident(
			venue=self.incident.venue,
			security_staff=self.request.user if self.request.user.security_firm_id is not None else None)
		instance = self.get_object()
		if self.incident.current_status != IncidentStatus.local_draft.value[0]:
			IncidentLog.objects.create(
				log_type=IncidentLogType.media_removed.value[0],
				incident=self.incident,
				user=self.request.user,
				old_data=IncidentMediaSerializer(
					instance,
					context={
						'request': self.request,
						'no_image': True}).data,
				updated_data=None
			)
		instance.delete()
		return success_response(message='Media removed successfully')


class ManagerList(CustomListModelMixin, CustomGenericView):
	"""
	This API is to list all the managers according to the current user while creating an incident
	"""
	serializer_class = UserSerializer
	queryset = CustomUser.objects.all()

	def get_queryset(self):
		qs = super().get_queryset()
		venue = filter_venue_qs(
			Venue.objects.all(), self.request.user).get(
			id=self.request.query_params['venue'])
		if self.request.user.user_type in [UserType.hospitality_admin.value[0],
										   UserType.venue_admin.value[0], UserType.venue_manager.value[0], UserType.venue_staff.value[0]]:
			qs = qs.filter(
				user_type=UserType.venue_manager.value[0],
				vm_venues__id=venue.id)
		elif self.request.user.user_type in [UserType.security_admin.value[0], UserType.security_manager.value[0], UserType.security_staff.value[0]]:
			qs = qs.filter(
				Q(user_type=UserType.venue_manager.value[0], vm_venues__id=venue.id) |
				Q(user_type=UserType.security_manager.value[0],
				  security_firm_id=self.request.user.security_firm_id)
			)
		return qs

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class IncidentLogList(CustomListModelMixin, CustomGenericView):
	queryset = IncidentLog.objects.select_related('user').all()
	serializer_class = IncidentLogSerializer

	def get_queryset(self):
		qs = super().get_queryset()
		incident = filter_incident_qs(
			Incident.objects.all(),
			self.request.user).values('id').get(
			id=self.request.query_params['incident'])
		return qs.filter(incident_id=incident['id'])

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class GrowthStat(CustomAPIView):
	permission_classes = (IsSuperUser,)

	def get(self, request):
		hospitality_subscription_ids = VenueSubscription.objects.values_list(
			'subscription_id', flat=True)
		security_subscription_ids = SecurityFirmSubscription.objects.values_list(
			'subscription_id', flat=True)
		total_hospitality_revenue = SubscriptionPayment.objects.filter(
			subscription_id__in=hospitality_subscription_ids).aggregate(
			Sum('amount'))['amount__sum']
		total_hospitality_admins = CustomUser.objects.filter(
			user_type=UserType.hospitality_admin.value[0]).count()
		total_venues = Venue.objects.count()
		total_venue_users = CustomUser.objects.filter(
			user_type__in=[
				UserType.venue_admin.value[0],
				UserType.venue_manager.value[0],
				UserType.venue_staff.value[0]]).count()
		total_hospitality_drafted_incidents = Incident.objects.filter(
			drafted_by__hospitality__isnull=False).count()
		total_hospitality_submitted_incidents = Incident.objects.filter(
			submitted_by__hospitality__isnull=False).count()
		total_hospitality_approved_incidents = Incident.objects.filter(
			approved_by__hospitality__isnull=False).count()
		total_security_revenue = SubscriptionPayment.objects.filter(
			subscription_id__in=security_subscription_ids).aggregate(
			Sum('amount'))['amount__sum']
		total_security_admins = CustomUser.objects.filter(
			user_type=UserType.security_admin.value[0]).count()
		total_security_users = CustomUser.objects.filter(
			user_type__in=[
				UserType.security_manager.value[0],
				UserType.security_staff.value[0]]).count()
		total_security_drafted_incidents = Incident.objects.filter(
			drafted_by__security_firm__isnull=False).count()
		total_security_submitted_incidents = Incident.objects.filter(
			submitted_by__security_firm__isnull=False).count()
		total_security_approved_incidents = Incident.objects.filter(
			approved_by__security_firm__isnull=False).count()
		result = {
			'total_hospitality_revenue': total_hospitality_revenue,
			'total_hospitality_admins': total_hospitality_admins,
			'total_venues': total_venues,
			'total_venue_users': total_venue_users,
			'total_hospitality_drafted_incidents': total_hospitality_drafted_incidents,
			'total_hospitality_submitted_incidents': total_hospitality_submitted_incidents,
			'total_hospitality_approved_incidents': total_hospitality_approved_incidents,
			'total_security_revenue': total_security_revenue,
			'total_security_admins': total_security_admins,
			'total_security_users': total_security_users,
			'total_security_drafted_incidents': total_security_drafted_incidents,
			'total_security_submitted_incidents': total_security_submitted_incidents,
			'total_security_approved_incidents': total_security_approved_incidents,
		}
		return success_response(data=result)


class ServiceProviderList(CustomListModelMixin, CustomGenericView):
	permission_classes = (IsSuperUser,)
	pagination_class = None
	serializer_class = ServiceProviderSerializer
	queryset = ServiceProvider.objects.all()

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)


class ServiceProviderAllocation(CustomAPIView):
	permission_classes = (IsSuperUser,)

	def get(self, request):
		paginator = CustomPagination()
		if request.query_params['type'] == 'H':
			model_class = Hospitality
			serializer_class = HospitalitySerializer
		elif request.query_params['type'] == 'V':
			model_class = Venue
			serializer_class = VenueListSerializer
		elif request.query_params['type'] == 'S':
			model_class = SecurityFirm
			serializer_class = SecurityFirmSerializer
		else:
			raise AttributeError
		qs = model_class.objects.select_related(
			'sso_service_provider').filter(sso_service_provider__isnull=False)
		paginated_qs = paginator.paginate_queryset(qs, request)
		serializer = serializer_class(
			paginated_qs, many=True, context={
				'request': request, 'service_provider': True})
		return paginated_success_response(
			data=paginator.get_paginated_response(serializer.data).data)

	def post(self, request):
		sso_service_provider = ServiceProvider.objects.get(entity=request.data['entity'])
		if request.data['type'] == 'H':
			Hospitality.objects.filter(
				id__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
			CustomUser.objects.filter(
				hospitality_id__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
		elif request.data['type'] == 'V':
			Venue.objects.filter(
				id__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
			CustomUser.objects.filter(
				va_venues__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
			CustomUser.objects.filter(
				vm_venues__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
			CustomUser.objects.filter(
				vs_venues__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
			hospitality_ids = Venue.objects.filter(
				id__in=request.data['ids']).values_list(
				'hospitality_id', flat=True)
			CustomUser.objects.filter(
				user_type=UserType.hospitality_admin.value[0],
				hospitality_id__in=hospitality_ids).update(
				sso_service_provider=sso_service_provider)
		elif request.data['type'] == 'S':
			SecurityFirm.objects.filter(
				id__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
			CustomUser.objects.filter(
				security_firm_id__in=request.data['ids']).update(
				sso_service_provider=sso_service_provider)
		else:
			raise AttributeError
		return success_response(message='Service provider allocated successfully')

	def delete(self, request):
		if request.data['type'] == 'H':
			Hospitality.objects.filter(
				id__in=request.data['ids']).update(
				sso_service_provider=None)
			CustomUser.objects.filter(
				hospitality_id__in=request.data['ids']).update(
				sso_service_provider=None)
		elif request.data['type'] == 'V':
			Venue.objects.filter(
				id__in=request.data['ids']).update(
				sso_service_provider=None)
			CustomUser.objects.filter(
				va_venues__in=request.data['ids']).update(
				sso_service_provider=None)
			CustomUser.objects.filter(
				vm_venues__in=request.data['ids']).update(
				sso_service_provider=None)
			CustomUser.objects.filter(
				vs_venues__in=request.data['ids']).update(
				sso_service_provider=None)
			hospitality_ids = Venue.objects.filter(
				id__in=request.data['ids']).values_list(
				'hospitality_id', flat=True)
			CustomUser.objects.filter(
				user_type=UserType.hospitality_admin.value[0],
				hospitality_id__in=hospitality_ids).update(
				sso_service_provider=None)
		elif request.data['type'] == 'S':
			SecurityFirm.objects.filter(
				id__in=request.data['ids']).update(
				sso_service_provider=None)
			CustomUser.objects.filter(
				security_firm_id__in=request.data['ids']).update(
				sso_service_provider=None)
		else:
			raise AttributeError
		return success_response(message='Service provider unallocated successfully')



class DownloadReportView(View):
	def get(self, request):
		token_data = TokenBackend(
			settings.SIMPLE_JWT['ALGORITHM'],
			settings.SIMPLE_JWT['SIGNING_KEY']).decode(request.GET['token'])
		token_data['key']
		file_bytes = io.BytesIO()
		bucket.download_fileobj(token_data['key'], file_bytes)
		response = HttpResponse(file_bytes.getvalue(), content_type='text/csv')
		response['Content-Disposition'] = f"attachment; filename={token_data['key'].replace('reports/', '')}"
		return response


class VerifyLicenseAPI(CustomAPIView):
	throttle_scope = 'license_verification'

	def get(self, request):
		if request.user.user_type in ALL_SECURITY_ROLES:
			expiry_date, license_type = validate_security_license(request.query_params['license_number'])
			return success_response(data={
				'expiry_date': expiry_date,
				'license_type': license_type
			})
		return success_response()


class CovidChecklistView(CustomGenericView):
	queryset = CovidChecklist.objects.all().select_related('user', 'venue')
	serializer_class = CovidChecklistSerializer
	filter_class = ChecklistFilter

	def get_queryset(self):
		qs = super().get_queryset()
		qs = filter_checklist_qs(qs, self.request.user)
		return qs

class CovidChecklistNewView(CustomGenericView):
	queryset = ChecklistNew.objects.all()
	serializer_class = CovidChecklistSerializerNew
	filter_class = ChecklistFilterNew

	def get_queryset(self):
		qs = super().get_queryset()
		qs = filter_checklist_qs_new(qs, self.request.user)
		return qs


class CovidChecklistNewSubmitView(CustomGenericView):
	queryset = ChecklistNewSubmited.objects.all().select_related('user')
	serializer_class = CovidChecklistSerializerNewSubmited
	filter_class = ChecklistFilterNewSubmited

	def get_queryset(self):
		qs = super().get_queryset()
		qs = filter_checklist_qs_new(qs, self.request.user)
		return qs




class CovidChecklistList(CustomListModelMixin, CustomCreateModelMixin, CovidChecklistView):
	@transaction.atomic
	def post(self, request, *args, **kwargs):
		print('post COVID CHECKLIST')
		return self.create(request, *args, **kwargs)
	
	def get(self, request, *args, **kwargs):
		print('get')
		if request.query_params.get('export') == 'True':
			qs = self.filter_queryset(self.get_queryset()).select_related(None).prefetch_related(None).values_list('id', flat=True)
			count = qs.count()
			if not count:
				raise CustomException('There is no checklist to be exported in the selected range')
			host = f"{'https' if request.is_secure() else 'http'}://{request.META['HTTP_HOST']}"
			create_checklist_report.apply_async(args=[
				qs.query.sql_with_params(),
				request.user.email,
				host,
				request.query_params['venue'],
				request.query_params['checklist_type'],
				request.query_params.get('created_after'),
				request.query_params.get('created_before'),
			])
			return success_response(message='You\'ll soon get the checklist report on your e-mail.', data={'count': count})
		self.queryset = self.queryset.defer('checklist_json')
		self.serializer_class = CovidChecklistShortSerializer
		return self.list(request, *args, **kwargs)


class CovidChecklistDetail(CustomRetrieveModelMixin, CovidChecklistView):
	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)


class ChecklistImageList(CustomCreateModelMixin, CustomGenericView):
	serializer_class = ChecklistImageSerializer
	queryset = ChecklistImage.objects.all()

	def post(self, request, *args, **kwargs):
		kwargs['message'] = None
		return self.create(request, *args, **kwargs)


#class ChecklistNewView(CustomListModelMixin, CustomCreateModelMixin, CovidChecklistNewView):
class ChecklistNewView(CustomListModelMixin,CovidChecklistNewView, CustomCreateModelMixin,CustomUpdateModelMixin):
	#lookup_field = 'id'
	
	def dictfetchall(self, cursor):
		"Return all rows from a cursor as a dict"
		columns = [col[0] for col in cursor.description]
		return [
			dict(zip(columns, row))
			for row in cursor.fetchall()
		]
		
	
	def post(self, request, *args, **kwargs):
		print('post REQUEST111')
		print(self)
		print(request.data['checklist_name'])
		
		
		
		try:
			print(request.data['checklist_recurring'])
		except KeyError:
			request.data['checklist_recurring']=''
		try:
			print(request.data['recuring_mon'])
		except KeyError:
			request.data['recuring_mon']=''
		try:
			print(request.data['recuring_tue'])
		except KeyError:
			request.data['recuring_tue']=''
		try:
			print(request.data['recuring_wed'])
		except KeyError:
			request.data['recuring_wed']=''
		try:
			print(request.data['recuring_fri'])
		except KeyError:
			request.data['recuring_fri']=''
		try:
			print(request.data['recuring_sat'])
		except KeyError:
			request.data['recuring_sat']=''
		try:
			print(request.data['recuring_sun'])
		except KeyError:
			request.data['recuring_sun']=''
		try:
			print(request.data['recuring_type'])
		except KeyError:
			request.data['recuring_type']=''
		try:
			print(request.data['recuring_start'])
		except KeyError:
			request.data['recuring_start']=''
		try:
			print(request.data['recuring_start_time'])
		except KeyError:
			request.data['recuring_start_time']=''			
		try:
			print(request.data['recuring_end_time'])
		except KeyError:
			request.data['recuring_end_time']=''					
		try:
			print(request.data['recuring_reminder'])
		except KeyError:
			request.data['recuring_reminder']=''					
			
			
			

		if (request.data['id'] !='' and request.data['id'] !=0 ):
			print('UPDATE')
			print(request.data['id'])
			print("OBJ update kwargs= %s , data = %s" % (kwargs,  str(request.data)))
			pk = request.data.get('id')
			if (kwargs.get('pk') is not None):
				kwargs['pk'] = request.data.get('id')
			self.kwargs['pk'] = request.data.get('id')
			return self.update(request, *args, **kwargs)
		else:
			record = ChecklistNew(checklist_name=request.data['checklist_name'], checklist_description=request.data['checklist_description'],
			checklist_json=request.data['checklist_json'],	 checklist_recurring=request.data['checklist_recurring'],	recuring_mon=request.data['recuring_mon'],
			recuring_tue=request.data['recuring_tue'],	  recuring_wed=request.data['recuring_wed'],	recuring_fri=request.data['recuring_fri']
			,
			recuring_sat=request.data['recuring_sat'],		   recuring_sun=request.data['recuring_sun'],	   recuring_type=request.data['recuring_type'],
			recuring_start=request.data['recuring_start'],		recuring_start_time=request.data['recuring_start_time'],	 recuring_end_time=request.data['recuring_end_time'],
			recuring_reminder=request.data['recuring_reminder']	   )
			record.save()
			if isinstance(request.data['selectedUser'], list):
				for val in request.data['selectedUser']:
					print(val)
					user=CustomUser.objects.get(id=val)
					print(user)
					record.users.add(user)
			else:
				print(request.data['selectedUser'])
				user=CustomUser.objects.get(id=request.data['selectedUser'])
				print(user)
				record.users.add(user)				
				
				
			return Response({'status': {'code': status.HTTP_200_OK,
										'message': kwargs.get('message', CREATE_SUCCESS)},
							 'data': request.data
							 })
			#return self.create(request, *args, **kwargs)


	def get(self, request, *args, **kwargs):
		print('get REQUEST')
		self.queryset = self.queryset.defer('checklist_json')
		self.serializer_class = CovidChecklistSerializerNew
		#return self.list(request, *args, **kwargs)
		paginate_by = 10
		'''all_entries = ChecklistNew.objects.all()
		paginator = Paginator(all_entries, 5) # Show 25 contacts per page.
		page_number = request.GET.get('page')
		all_entries = paginator.get_page(page_number)		
		export=[]
		for val in all_entries:
			print(val)
			allUsers =val.users.all()
			allUserDict=[]
			for user in allUsers:
				#print(user.id)
				allUserDict.append({'email':user.email, 'nameOfHospitality':user.hospitality.name, 'venueName':'','userName':user.first_name + ' ' + user.last_name ,'profilePicture':'','role':user.user_type})
				#allUserDict.append(model_to_dict(user))
			export.append({'user':allUserDict,'data':{'id':val.id, 'checklist_name': val.checklist_name, 'checklist_description': val.checklist_description, 'checklist_json':val.checklist_json, 'created_at':val.created_at } })
		print(export)
		'''
		c = connection.cursor()
		selectedUser = request.GET.get('selectedUser')
		if( selectedUser != None and selectedUser !=''):
			userFilter = " and api_customuser.id= '"+selectedUser+"'"
		else:
			userFilter = " "
		try:
			c.execute('''select api_checklistnew.checklist_description, api_checklistnew.checklist_name, api_checklistnew.id,
						 api_checklistnew.checklist_json,api_checklistnew.created_at,
						 api_customuser.email, api_customuser.profile_picture, api_customuser.user_type as role, api_customuser.first_name, api_customuser.last_name,	api_venue.name AS venueName,
						 api_hospitality.name as nameOfHospitality

						 from api_checklistnew
						 left join api_checklistnew_users on api_checklistnew_users.checklistnew_id = api_checklistnew.id
						 left join api_customuser on api_customuser.id = api_checklistnew_users.customuser_id
						 left join api_venue_venue_managers on api_venue_venue_managers.customuser_id = api_customuser.id
						 left join api_venue on api_venue.id = api_venue_venue_managers.venue_id
						 left join api_hospitality on api_hospitality.id = api_customuser.hospitality_id
						 where api_customuser.first_name != 'null'
						 ''' + userFilter)
			result = self.dictfetchall(c)
			export={}
			for record in result:
				print(record)
				uId = str(record['id'])
				print(uId)
				uId = uId.replace("-", "")
				print(uId)
				try:
					print(export[uId])
				except KeyError:
					export[uId]={'user':[],'data':{'id':str(record['id']), 'checklist_name': record['checklist_name'], 'checklist_description': record['checklist_description'], 'checklist_json':record['checklist_json'], 'created_at':record['created_at'] }}
					
				export[uId]['user'].append({'email':record['email'], 'nameOfHospitality':record['nameofhospitality'], 'venueName':record['venuename'],'userName':record['first_name'] + ' ' + record['last_name'] ,
				'profilePicture':'', 'role':record['role']})

		finally:
			c.close()
		#return Response({'status': {'code': status.HTTP_200_OK}, 'results':export, 'count': paginator.count, 'next': all_entries.has_next(), 'previous': all_entries.has_previous() })
		return Response({'status': {'code': status.HTTP_200_OK}, 'results':export })


	def delete(self, request, *args, **kwargs):
		print('delete REQUEST')
		print(request.data['itemId'])
		ChecklistNew.objects.filter(id=request.data['itemId']).delete()
		result = {'status': {'code': status.HTTP_200_OK,
									 'message': None}
						  }
		return Response(result)







	#lookup_field = 'id'



class ChecklistNewSubmitView(CustomListModelMixin, CovidChecklistNewSubmitView, CustomCreateModelMixin,CustomUpdateModelMixin):
	def post(self, request, *args, **kwargs):
		print('post request')
		print(self)
		print(request.data['checklist_name'])
		if (request.data['id'] !=''):
			print('UPDATE')
			print(request.data['id'])
			print("OBJ update kwargs= %s , data = %s" % (kwargs,  str(request.data)))
			pk = request.data.get('id')
			if (kwargs.get('pk') is not None):
				kwargs['pk'] = request.data.get('id')
			self.kwargs['pk'] = request.data.get('id')
			return self.update(request, *args, **kwargs)
		else:
			return self.create(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		print('get')
		c = connection.cursor()
		allRecords=[]
		current_user = request.user
		print (current_user.id)
		print(datetime.datetime.today().weekday())
		try:
			userId=current_user.id
			userId="0b4b2cdc-053e-4299-b10a-e84ac5d622a9"
			c.execute('''select api_checklistnew.id, api_checklistnew.checklist_name, api_checklistnew.checklist_description from api_checklistnew
						left join api_checklistnew_users on api_checklistnew_users.checklistnew_id = api_checklistnew.id
						left join api_customuser on api_checklistnew_users.customuser_id = api_customuser.id

						where api_checklistnew_users.customuser_id = %s
						and api_checklistnew.recuring_type='once'
						and api_checklistnew.recuring_start='2020-08-12'
						and api_checklistnew.recuring_start_time < CURRENT_TIME	 
						and api_checklistnew.recuring_end_time > CURRENT_TIME  ''',[userId])
			result = c.fetchall()
			for record in result:
				allRecords.append({'type':'once', 'id':record[0], 'checklistName':record[1], 'checklistDescription':record[2]})
			
			days2Filters={0:"and api_checklistnew.recuring_mon='True'", 1:"and api_checklistnew.recuring_tue='True'",2:"and api_checklistnew.recuring_wed='True'",3:"and api_checklistnew.recuring_fri='True'",4:"and api_checklistnew.recuring_fri='True'",5:"and api_checklistnew.recuring_sat='True'",6:"and api_checklistnew.recuring_sun='True'"}


			
		finally:
			c.close()
		print(allRecords)
		self.queryset = self.queryset.defer('checklist_json')
		self.serializer_class = CovidChecklistSerializerNewSubmited
		return self.list(request, *args, **kwargs)

class ActivityListView(CustomListModelMixin, CovidChecklistNewSubmitView, CustomCreateModelMixin,CustomUpdateModelMixin):
	def get(self, request, *args, **kwargs):
		print('get')
		c = connection.cursor()
		allRecords={'08':[],'816':[],'1624':[]}
		current_user = request.user
		print (current_user.id)
		print(datetime.datetime.today().weekday())
		today = datetime.datetime.today().strftime('%Y-%m-%d')
		try:
			c.execute('''select api_checklistnew.id, api_checklistnew.checklist_name, api_checklistnew.checklist_description ,api_checklistnew.recuring_start_time, api_checklistnew.recuring_end_time, api_checklistnew.recuring_type, api_venue.name AS venueName,api_checklistnew.checklist_json
						from api_checklistnew
						left join api_checklistnew_users on api_checklistnew_users.checklistnew_id = api_checklistnew.id
						left join api_customuser on api_checklistnew_users.customuser_id = api_customuser.id
						left join api_venue_venue_managers on api_venue_venue_managers.customuser_id = api_customuser.id
                        left join api_venue on api_venue.id = api_venue_venue_managers.venue_id
						where api_checklistnew.recuring_type='once'
						and api_checklistnew.recuring_start=%s 
						UNION
						select api_checklistnew.id, api_checklistnew.checklist_name, api_checklistnew.checklist_description   ,api_checklistnew.recuring_start_time, api_checklistnew.recuring_end_time, api_checklistnew.recuring_type, api_venue.name AS venueName,api_checklistnew.checklist_json
						from api_checklistnew
						left join api_checklistnew_users on api_checklistnew_users.checklistnew_id = api_checklistnew.id
						left join api_customuser on api_checklistnew_users.customuser_id = api_customuser.id
						left join api_venue_venue_managers on api_venue_venue_managers.customuser_id = api_customuser.id
                        left join api_venue on api_venue.id = api_venue_venue_managers.venue_id
						where  api_checklistnew.recuring_type!='once'
						and api_checklistnew.recuring_start<=%s


						ORDER BY  recuring_start_time DESC
						
						
						
						''',[today, today])
			result = c.fetchall()
			print(result)
			now = datetime.datetime.now()
			for record in result:
				end_datetime = record[4]
				print(record[4])
				print(record[4].hour)
				my_datetime = now.replace(hour=end_datetime.hour, minute=end_datetime.minute, second=end_datetime.second, microsecond=0)
				if (now > my_datetime):
					outdated=True
				else:
					outdated =False
					
					
				if(record[4].hour < 8):
					allRecords['08'].append({'type':record[5], 'id':record[0], 'checklistName':record[1], 'checklistDescription':record[2], 'outdated':outdated, 'venueName':record[6], 'json':record[7]})
				elif (record[4].hour < 16):
					allRecords['816'].append({'type':record[5], 'id':record[0], 'checklistName':record[1], 'checklistDescription':record[2], 'outdated':outdated, 'venueName':record[6], 'json':record[7]})
				else:
					allRecords['1624'].append({'type':record[5], 'id':record[0], 'checklistName':record[1], 'checklistDescription':record[2], 'outdated':outdated, 'venueName':record[6], 'json':record[7]})




		finally:
			c.close()
		return Response({'status': {'code': status.HTTP_200_OK}, 'results':allRecords, 'count': len(result) })
