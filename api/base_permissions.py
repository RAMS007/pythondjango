from rest_framework.permissions import BasePermission
from api.choices import UserType


class IsSuperUser(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                return True
        return False


class CanAccessUsers(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            if (request.user.is_superuser or request.user.user_type in [x.value[0] for x in [UserType.hospitality_admin,
                                                                                             UserType.venue_admin,
                                                                                             UserType.venue_manager,
                                                                                             UserType.security_admin,
                                                                                             UserType.security_manager]]):
                return True
        return False


class CanMakePayments(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            if (request.user.user_type in [x.value[0] for x in [
                    UserType.hospitality_admin, UserType.security_admin]]):
                return True
        return False


class IsSecurityAdmin(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return request.user.user_type == UserType.security_admin.value[0]
        return False


class IsHospitalityAdmin(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return request.user.user_type == UserType.hospitality_admin.value[0]
        return False


class CanViewVenueDoc(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return (request.user.user_type in [
                    UserType.hospitality_admin.value[0], UserType.venue_admin.value[0]]) or request.user.is_superuser
        return False


class CanUpdateVenue(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            if request.user.user_type in [
                    UserType.hospitality_admin.value[0], UserType.venue_admin.value[0]]:
                return True
        return False


class CanAssignSecurityStaff(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            if request.user.user_type in [
                    UserType.security_admin.value[0], UserType.security_manager.value[0]]:
                self.message = 'Your firm is not subscribed. Please subscribe to assign staff to venues.'
                if request.user.security_firm.current_subscription_status == 'active':
                    return True
        return False


class CanAssignVenueStaff(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            if request.user.user_type in [
                    UserType.venue_admin.value[0], UserType.hospitality_admin.value[0], UserType.venue_manager.value[0]]:
                return True
        return False
