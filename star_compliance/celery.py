import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'star_compliance.settings')

app = Celery('star_compliance')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
