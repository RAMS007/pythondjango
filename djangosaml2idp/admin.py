from django.contrib import admin
from djangosaml2idp.models import ServiceProvider


admin.site.register(ServiceProvider)
