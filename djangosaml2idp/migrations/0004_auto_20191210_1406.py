# Generated by Django 2.2.5 on 2019-12-10 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('djangosaml2idp', '0003_auto_20191210_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceprovider',
            name='login_url',
            field=models.URLField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='serviceprovider',
            name='name',
            field=models.CharField(default='', max_length=250),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='serviceprovider',
            name='entity',
            field=models.CharField(help_text='Enter Entity ID URL', max_length=255, primary_key=True, serialize=False),
        ),
    ]
