from django.apps import AppConfig


class CustomAppConfig(AppConfig):
    name = 'djangosaml2idp'
    verbose_name = 'SAML'    

    def ready(self):
        from api import signals