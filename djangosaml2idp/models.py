import copy
import json

from django.conf import settings
from django.db import models


class ServiceProvider(models.Model):
    name = models.CharField(max_length=250)
    login_url = models.URLField()
    entity = models.CharField(max_length=255, primary_key=True, help_text='Enter Entity ID URL')
    processor = models.CharField(max_length=255, null=True)
    attribute_mapping = models.TextField(null=True)
    metadata = models.TextField()

    def __str__(self):
        return self.name

    def get_attribute_mapping(self):
        if not self.attribute_mapping:
            return settings.SAML_DEFAULT_MAPPING

        return json.loads(self.attribute_mapping)

    def get_processor(self):
        if not self.processor:
            return settings.SAML_DEFAULT_PROCESSOR

        return self.processor

    # def get_metadata(self):
    #     return settings.SAML_DEFAULT_METADATA.format(entity=self.entity)

    def as_value(self):
        return {
            'processor': self.get_processor(),
            'attribute_mapping': self.get_attribute_mapping()
        }

    @classmethod
    def get_config(cls):
        return {
            service.entity: service.as_value()
            for service in cls.objects.all()
        }


class IdentityProvider(object):
    @classmethod
    def get_config(cls):
        idp_config = copy.deepcopy(settings.SAML_DEFAULT_IDP_CONFIG)
        idp_config['metadata'] = {
            'inline': [
                service.metadata for service in ServiceProvider.objects.all()
            ]
        }
        return idp_config
