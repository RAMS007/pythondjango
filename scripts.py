from api.models import Incident, IncidentLog

def update_incident_dates():
    for i in IncidentLog.objects.filter(log_type=0):
        Incident.objects.filter(id=i.incident_id).update(drafted_at=i.created_at)
    for i in IncidentLog.objects.filter(log_type=1):
        Incident.objects.filter(id=i.incident_id).update(submitted_at=i.created_at)
    for i in IncidentLog.objects.filter(log_type=2):
        Incident.objects.filter(id=i.incident_id).update(approved_at=i.created_at)
